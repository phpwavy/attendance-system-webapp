<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\UserLogController;
use App\Http\Controllers\Admin\holidayController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\LeaveController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});




Route::group(['middleware' => ['auth', 'isAdmin' ]], function () {
Route::resources([
    // users
    'users' => UserController::class,
    'holidays' => holidayController::class,
    'roles' => RolesController::class,
]);

// Show Trashed users
Route::get('/only-trashed-users', [UserController::class, 'only_trashed_user'])->name('only_trashed_user');

// Recover user
Route::get('/recover-user/{id}', [UserController::class, 'recover_user'])->name('recover_user');

// Delete User Permanently
Route::post('/remove-user-permanently', [UserController::class, 'remove_user_permanently'])->name('remove_user_permanently');

// Logs of specific user
Route::get('/user-logs/{id}', [UserLogController::class, 'show'])->name('user_logs');
Route::get('/user-report/{id}', [ReportController::class, 'report'])->name('user_report');

// All User Attendance Report
Route::get('/all-attendance-report', [ReportController::class, 'all_report'])->name('all_user_report');

// All leave requests applied by the user
Route::get('/all-leave-request', [LeaveController::class, 'all_leave_request'])->name('all_leave_request');

// Change Request Status - will show detail of leave and form
Route::get('/change-request-status/{id}', [LeaveController::class, 'change_request_status'])->name('change_request_status');

// update request status by admin
Route::put('/update-request-status/{id}', [LeaveController::class, 'udpate_request_status'])->name('update-request-status');

// Update/add user Attendance by admin
Route::get('/attendances', [AttendanceController::class, 'all_attendance'])->name('attendances.index');
Route::get('/attendance/{id}/edit', [AttendanceController::class, 'edit_attendance'])->name('attendances.edit');
Route::get('/attendance/create', [AttendanceController::class, 'create_attendance'])->name('attendance.create');
Route::delete('/attendance/{id}', [AttendanceController::class, 'destroy_attendance'])->name('attendance.destroy');
Route::post('/attendance/add', [AttendanceController::class, 'store_attendance'])->name('attendance.store');
// show log report
Route::get('/log-report', [ReportController::class, 'show_log_report'])->name('log_report');


});




// Route::get('/sent-mail', [LeaveController::class, 'sent_mail']);


Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', [App\Http\Controllers\PageController::class, 'dashboard'])->name('dashboard');
    // Route::post('/activity',[App\Http\Controllers\AttendanceController::class, 'Activity'])->name('activity');
});

Route::get('/get-working-hours', [AttendanceController::class, 'get_working_hours'])->name('get_working_hours');

Route::get('/clear-cache', function() {
    Artisan::call('optimize:clear');
    echo Artisan::output();
});

// Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
// Route::get('dashboard', function(){
//     return "DASHBOARD";
// })->name('dashboard');
Route::get('download', [App\Http\Controllers\PageController::class, 'downloadExcel']);
Auth::routes();


