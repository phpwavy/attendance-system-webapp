<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\LeaveController;
use App\Http\Controllers\Admin\ReportController;
/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
|
| Here is where you can register users routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "users" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth'], function () {
    Route::post('/activity', [ AttendanceController::class, 'activity'])->name('activity');
    Route::get('/apply-leave-request', [ LeaveController::class, 'apply_leave_request' ])->name('apply_leave_request');
    Route::get('/leave-applied', [LeaveController::class,  'index'])->name('leave-applied');
    Route::post('/store-leave-request', [LeaveController::class, 'store_leave_request'])->name('store-leave-request');
    Route::get('/cancel_request/{id}', [LeaveController::class, 'cancel_request'])->name('cancel_request');
    Route::get('log-report', [ReportController::class, 'show_user_log'])->name('show_user_log');
    Route::get('/attendance-report', [ReportController::class, 'show_user_report'])->name('show_user_report');
});








