-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 18, 2023 at 10:36 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mytracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_details`
--

CREATE TABLE `attendance_details` (
  `id` int UNSIGNED NOT NULL,
  `users_id` int DEFAULT NULL,
  `office_in` datetime DEFAULT NULL,
  `office_out` datetime DEFAULT NULL,
  `lunch_in` datetime DEFAULT NULL,
  `lunch_out` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendance_details`
--

INSERT INTO `attendance_details` (`id`, `users_id`, `office_in`, `office_out`, `lunch_in`, `lunch_out`, `created_at`, `updated_at`) VALUES
(1, 4, '2022-12-15 10:59:17', '2022-12-15 17:59:32', '2022-12-15 13:30:41', '2022-12-15 14:00:05', '2022-12-15 05:29:17', '2022-12-15 12:29:32'),
(14, 5, '2022-12-26 17:32:06', NULL, NULL, NULL, '2022-12-26 12:02:06', '2022-12-26 12:02:06'),
(15, 4, '2022-12-27 18:45:47', NULL, NULL, NULL, '2022-12-27 13:15:47', '2022-12-27 13:15:47'),
(20, 4, '2023-01-04 18:19:16', NULL, '2023-01-04 18:19:25', NULL, '2023-01-04 12:49:16', '2023-01-04 12:49:25'),
(35, 4, '2023-01-09 17:19:30', '2023-01-09 17:35:41', '2023-01-09 17:23:12', '2023-01-09 17:32:40', '2023-01-09 11:49:30', '2023-01-09 12:05:41'),
(38, 5, '2023-01-17 10:30:25', NULL, NULL, NULL, '2023-01-17 05:00:25', '2023-01-17 05:00:25'),
(40, 8, '2023-01-18 09:57:29', NULL, '2023-01-18 10:53:52', '2023-01-18 12:39:10', '2023-01-18 04:27:29', '2023-01-18 07:09:10');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` int UNSIGNED NOT NULL,
  `event` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_date` date DEFAULT NULL,
  `status` enum('yes','no') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`id`, `event`, `event_date`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Facilis minima dolor', '1973-06-08', 'no', '2022-12-15 23:52:56', '2022-12-15 23:52:56'),
(3, 'Enjoy', '2023-01-26', 'no', '2022-12-16 02:04:59', '2022-12-16 02:04:59');

-- --------------------------------------------------------

--
-- Table structure for table `inter_missions`
--

CREATE TABLE `inter_missions` (
  `id` int UNSIGNED NOT NULL,
  `break_in` datetime DEFAULT NULL,
  `break_out` datetime DEFAULT NULL,
  `attendance_details_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inter_missions`
--

INSERT INTO `inter_missions` (`id`, `break_in`, `break_out`, `attendance_details_id`, `created_at`, `updated_at`) VALUES
(1, '2022-12-15 12:32:59', '2022-12-15 12:42:59', 1, '2022-12-15 07:02:59', '2022-12-15 07:12:59'),
(2, '2022-12-15 16:32:59', '2022-12-15 16:44:59', 1, '2022-12-15 11:02:59', '2022-12-15 11:14:59');

-- --------------------------------------------------------

--
-- Table structure for table `leave_requests`
--

CREATE TABLE `leave_requests` (
  `id` int UNSIGNED NOT NULL,
  `users_id` int DEFAULT NULL,
  `leave_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `which_half` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '1 => Pending, 2 => Processing, 3 => Reject, 4 => Approved',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leave_requests`
--

INSERT INTO `leave_requests` (`id`, `users_id`, `leave_type`, `start_date`, `end_date`, `which_half`, `start_time`, `end_time`, `reason`, `status`, `created_at`, `updated_at`, `msg`) VALUES
(1, 4, 'Full', '2023-01-02', '2023-01-02', NULL, NULL, NULL, 'TEST', '5', '2023-01-02 07:25:41', '2023-01-03 04:29:24', NULL),
(2, 4, 'Short', '2023-01-02', NULL, NULL, '14:56', '16:00', 'TEST', '1', '2023-01-02 07:26:19', '2023-01-02 07:26:19', NULL),
(3, 4, 'Half', '2023-01-02', NULL, 'second', NULL, NULL, 'TEST', '1', '2023-01-02 07:27:30', '2023-01-02 07:27:30', NULL),
(4, 4, 'Short', '2023-01-02', NULL, NULL, '13:59', '16:02', 'Reason', '1', '2023-01-02 07:29:00', '2023-01-02 07:29:00', NULL),
(5, 4, 'Short', '2023-01-02', NULL, NULL, '15:19', '16:20', 'Reason', '1', '2023-01-02 09:49:40', '2023-01-02 09:49:40', NULL),
(6, 4, 'Short', '2023-01-02', NULL, NULL, '15:19', '16:20', 'Reason', '1', '2023-01-02 09:50:11', '2023-01-02 09:50:11', NULL),
(7, 4, 'Short', '2023-01-02', NULL, NULL, '15:19', '16:20', 'Reason', '3', '2023-01-02 09:50:20', '2023-01-03 07:42:52', 'YES'),
(8, 4, 'Short', '2023-01-02', NULL, NULL, '15:19', '16:20', 'Reason', '2', '2023-01-02 09:50:21', '2023-01-03 10:26:52', 'fdafd'),
(9, 4, 'Short', '2023-01-02', NULL, NULL, '15:19', '16:20', 'Reason', '3', '2023-01-02 09:51:06', '2023-01-03 10:14:35', 'afsd'),
(10, 4, 'Full', '2023-01-02', '2023-01-02', NULL, NULL, NULL, 'sfa', '3', '2023-01-02 10:40:48', '2023-01-03 07:46:39', 'rejected'),
(11, 4, 'Full', '2023-01-02', '2023-01-02', NULL, NULL, NULL, 'reas', '4', '2023-01-02 10:45:56', '2023-01-03 07:43:46', 'Approved'),
(12, 4, 'Full', '2023-01-04', '2023-01-04', NULL, NULL, NULL, 'reasomn', '1', '2023-01-04 09:20:53', '2023-01-04 09:20:53', NULL),
(13, 4, 'Full', '2023-01-04', '2023-01-04', NULL, NULL, NULL, 'reasomn', '1', '2023-01-04 09:21:08', '2023-01-04 09:21:08', NULL),
(14, 4, 'Full', '2023-01-04', '2023-01-04', NULL, NULL, NULL, 'REaso', '1', '2023-01-04 09:23:41', '2023-01-04 09:23:41', NULL),
(15, 4, 'Full', '2023-01-04', '2023-01-04', NULL, NULL, NULL, 'reason', '1', '2023-01-04 09:25:39', '2023-01-04 09:25:39', NULL),
(16, 4, 'Full', '2023-01-04', '2023-01-04', NULL, NULL, NULL, 'reason', '1', '2023-01-04 09:26:58', '2023-01-04 09:26:58', NULL),
(17, 4, 'Full', '2023-01-04', '2023-01-04', NULL, NULL, NULL, 'reason', '1', '2023-01-04 09:28:52', '2023-01-04 09:28:52', NULL),
(18, 4, 'Full', '2023-01-04', '2023-01-04', NULL, NULL, NULL, 'reas', '1', '2023-01-04 09:29:17', '2023-01-04 09:29:17', NULL),
(19, 4, 'Full', '2023-01-04', '2023-01-04', NULL, NULL, NULL, 'asfads', '1', '2023-01-04 09:29:43', '2023-01-04 09:29:43', NULL),
(20, 4, 'Short', '2023-01-04', NULL, NULL, '15:01', '16:03', 'reasnfsaojdfoja', '4', '2023-01-04 09:31:56', '2023-01-04 10:15:05', 'TESTING MESSAGE WHICH WILL CLIENT WRITE'),
(21, 5, 'Full', '2023-01-13', '2023-01-13', NULL, NULL, NULL, 'test', '4', '2023-01-13 03:54:11', '2023-01-13 03:56:46', NULL),
(22, 5, 'Full', '2023-01-13', '2023-01-13', NULL, NULL, NULL, 'test', '4', '2023-01-13 03:57:03', '2023-01-13 05:14:17', 'update'),
(23, 5, 'Full', '2023-01-13', '2023-01-13', NULL, NULL, NULL, 'test', '4', '2023-01-13 05:15:23', '2023-01-13 05:39:09', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(7, '2014_10_12_000000_create_users_table', 1),
(8, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2019_08_19_000000_create_failed_jobs_table', 1),
(10, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(11, '2022_12_09_070332_create_user_details_table', 1),
(12, '2022_12_09_070519_created_profile_image_and_emergency_contact_name_and_emergency_contact_relation_and_emergency_contact_phone_column_in_user_details_table', 1),
(13, '2022_12_12_131351_created_last_login_at_and_last_login_ip_column_in_users_table', 2),
(14, '2022_12_14_045742_create_user_logs_table', 3),
(15, '2022_12_14_052432_created_join_date_column_in_user_details_table', 4),
(16, '2022_12_16_045614_create_holidays_table', 5),
(17, '2022_12_16_063440_create_attendance_details_table', 6),
(19, '2022_12_16_064842_create_inter_missions_table', 7),
(20, '2022_12_28_124339_create_roles_table', 8),
(21, '2022_12_28_154031_created_roles_id_column_in_users_table', 9),
(22, '2022_12_29_131923_created_country_and_city_and_state_column_in_user_details_table', 10),
(23, '2023_01_02_112200_create_leave_requests_table', 11),
(24, '2023_01_03_113325_created_msg_column_in_leave_requests_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int UNSIGNED NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Role4', '2022-12-28 07:30:17', '2022-12-28 09:20:31'),
(3, 'Role1', '2022-12-28 07:30:42', '2022-12-28 07:30:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int NOT NULL DEFAULT '1' COMMENT '1 => User, 2 => Admin',
  `status` int NOT NULL DEFAULT '1' COMMENT '1 => Active, 2 => Inactive',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `last_login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roles_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `status`, `remember_token`, `deleted_at`, `created_at`, `updated_at`, `last_login_at`, `last_login_ip`, `roles_id`) VALUES
(3, 'Admin', 'admin@gmail.com', NULL, '$2y$10$KycPDQYZ3lnQv3g1Le9gteKtDJUrZFFLDyHgiPlVYpMKz6W640iw6', 2, 1, NULL, NULL, '2022-12-12 06:32:34', '2023-01-18 04:10:01', '2023-01-18 09:40:01', '::1', NULL),
(4, 'Ariana Hunter', 'wyfah@mailinator.com', NULL, '$2y$10$xgTalohwtEvy05dvuUuOae3OuDJ4hU6uS7O7/BdPdpOSljSz7eQjG', 1, 1, NULL, NULL, '2022-12-12 06:35:39', '2023-01-16 05:04:12', '2023-01-16 10:34:12', '::1', '3'),
(5, 'Carol Warner', 'mimy@gmail.com', NULL, '$2y$10$KycPDQYZ3lnQv3g1Le9gteKtDJUrZFFLDyHgiPlVYpMKz6W640iw6', 1, 1, NULL, NULL, '2022-12-14 00:27:04', '2023-01-18 04:11:11', '2023-01-18 09:41:11', '::1', NULL),
(6, 'Jerome Barr', 'gefe@mailinator.com', NULL, '$2y$10$QsNKFFV9Pg16Ag7FwJI3WupN8C60eK5DutICBL8PC4mLvO2AOMHXG', 1, 2, NULL, NULL, '2022-12-28 11:11:47', '2022-12-28 11:11:47', NULL, NULL, '3'),
(7, 'Carol Doyle', 'cide@mailinator.com', NULL, '$2y$10$VqWANtfQmCp3PCNCu535v.gHvGvS27z99Lr12xTveYvs9j.kvegum', 1, 1, NULL, NULL, '2022-12-28 11:15:27', '2022-12-28 11:15:27', NULL, NULL, '3'),
(8, 'Clare Galloway', 'quwosiryry@mailinator.com', NULL, '$2y$10$KycPDQYZ3lnQv3g1Le9gteKtDJUrZFFLDyHgiPlVYpMKz6W640iw6', 1, 1, NULL, NULL, '2022-12-29 07:52:41', '2023-01-18 04:27:26', '2023-01-18 09:57:26', '::1', '3');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int UNSIGNED NOT NULL,
  `users_id` int DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `phone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `profile_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_contact_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_contact_relation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_contact_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `users_id`, `address`, `phone`, `created_at`, `updated_at`, `profile_image`, `emergency_contact_name`, `emergency_contact_relation`, `emergency_contact_phone`, `join_date`, `country`, `city`, `state`) VALUES
(3, 4, 'Reiciendis eos quo o', '+1 (135) 188-6406', '2022-12-12 06:35:39', '2022-12-28 11:24:35', 'uploads/profile/16708467394.jpg', 'Kimberly Sellers', 'Id natus et quis ve', '+1 (746) 637-4507', '2022-12-05', NULL, NULL, NULL),
(4, 5, 'Sed tempore qui inc', '+1 (212) 545-3973', '2022-12-14 00:27:04', '2022-12-14 00:27:04', 'uploads/profile/16709974246.jpg', 'Olympia Collins', 'Sunt adipisci nulla', '+1 (976) 579-4288', '2011-04-13', NULL, NULL, NULL),
(5, 6, 'Asperiores at quo ha', '+1 (777) 636-7025', '2022-12-28 11:11:48', '2022-12-28 11:11:48', 'uploads/profile/167222590710.jpg', 'Uriah Conway', 'Optio qui voluptate', '+1 (237) 157-4353', '1997-11-28', NULL, NULL, NULL),
(6, 7, 'Rem minus non ad ex', '+1 (985) 852-7388', '2022-12-28 11:15:27', '2022-12-28 11:15:27', 'uploads/profile/16722261271.png', 'Elmo Fisher', 'Anim omnis dolor vol', '+1 (267) 489-7187', '1979-04-21', NULL, NULL, NULL),
(7, 8, 'Magna doloribus faci', '+1 (793) 252-7737', '2022-12-29 07:52:41', '2023-01-03 11:51:51', 'uploads/profile/16723003615.gif', 'Jessica Cantrell', 'Dolore eligendi vel', '+1 (584) 878-4056', '1996-01-24', 'United States', 'Fsm', 'Federated States Of Micronesia');

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` int UNSIGNED NOT NULL,
  `users_id` int DEFAULT NULL,
  `ip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_logs`
--

INSERT INTO `user_logs` (`id`, `users_id`, `ip_address`, `device_id`, `created_at`, `updated_at`) VALUES
(1, 3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '2022-12-13 23:37:41', '2022-12-13 23:37:41'),
(2, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-13 23:45:25', '2022-12-13 23:45:25'),
(3, 3, '0', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-13 23:48:42', '2022-12-13 23:48:42'),
(4, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-14 07:53:20', '2022-12-14 07:53:20'),
(5, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-14 23:24:54', '2022-12-14 23:24:54'),
(6, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-15 06:49:47', '2022-12-15 06:49:47'),
(7, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-15 23:47:56', '2022-12-15 23:47:56'),
(8, 4, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-16 00:25:46', '2022-12-16 00:25:46'),
(9, 4, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-16 01:55:36', '2022-12-16 01:55:36'),
(10, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-16 02:04:24', '2022-12-16 02:04:24'),
(11, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-16 06:53:30', '2022-12-16 06:53:30'),
(12, 4, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-16 06:53:41', '2022-12-16 06:53:41'),
(13, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-16 06:58:11', '2022-12-16 06:58:11'),
(14, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-16 07:11:53', '2022-12-16 07:11:53'),
(15, 3, '127.0.0.1', '2C-6D-C1-4D-98-7D   Media disconnected', '2022-12-19 05:09:26', '2022-12-19 05:09:26'),
(16, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-22 00:49:42', '2022-12-22 00:49:42'),
(17, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-22 11:44:40', '2022-12-22 11:44:40'),
(18, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-23 04:43:29', '2022-12-23 04:43:29'),
(19, 5, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-23 07:21:36', '2022-12-23 07:21:36'),
(20, 5, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-23 07:40:33', '2022-12-23 07:40:33'),
(21, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-23 10:08:51', '2022-12-23 10:08:51'),
(22, 5, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-26 11:29:00', '2022-12-26 11:29:00'),
(23, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-27 05:05:19', '2022-12-27 05:05:19'),
(24, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-27 13:14:45', '2022-12-27 13:14:45'),
(25, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-28 05:00:45', '2022-12-28 05:00:45'),
(26, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-28 07:10:24', '2022-12-28 07:10:24'),
(27, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-29 04:55:58', '2022-12-29 04:55:58'),
(28, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-30 05:04:32', '2022-12-30 05:04:32'),
(29, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-30 05:06:44', '2022-12-30 05:06:44'),
(30, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2022-12-30 07:27:45', '2022-12-30 07:27:45'),
(31, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-02 05:14:39', '2023-01-02 05:14:39'),
(32, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-02 05:15:06', '2023-01-02 05:15:06'),
(33, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-02 09:40:39', '2023-01-02 09:40:39'),
(34, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-02 10:23:22', '2023-01-02 10:23:22'),
(35, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-03 04:16:52', '2023-01-03 04:16:52'),
(36, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-03 04:17:11', '2023-01-03 04:17:11'),
(37, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-03 05:57:57', '2023-01-03 05:57:57'),
(38, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-03 10:14:23', '2023-01-03 10:14:23'),
(39, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-03 11:46:50', '2023-01-03 11:46:50'),
(40, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-04 09:20:24', '2023-01-04 09:20:24'),
(41, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-04 10:08:45', '2023-01-04 10:08:45'),
(42, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-04 12:11:52', '2023-01-04 12:11:52'),
(43, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-09 06:52:56', '2023-01-09 06:52:56'),
(44, 4, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-09 06:53:14', '2023-01-09 06:53:14'),
(45, 3, '127.0.0.1', 'C0-25-A5-95-3B-7D   \\Device\\Tcpip_{54829AC4-2E93-4865-A496-4E08311AA34F}', '2023-01-10 04:23:04', '2023-01-10 04:23:04'),
(46, 5, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-13 09:16:34', '2023-01-13 09:16:34'),
(47, 5, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-13 09:34:51', '2023-01-13 09:34:51'),
(48, 3, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-16 05:03:36', '2023-01-16 05:03:36'),
(49, 4, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-16 05:04:13', '2023-01-16 05:04:13'),
(50, 5, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-16 06:56:08', '2023-01-16 06:56:08'),
(51, 5, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-16 09:28:09', '2023-01-16 09:28:09'),
(52, 5, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-16 09:59:02', '2023-01-16 09:59:02'),
(53, 3, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-17 04:09:38', '2023-01-17 04:09:38'),
(54, 5, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-17 05:00:23', '2023-01-17 05:00:23'),
(55, 3, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-17 12:23:16', '2023-01-17 12:23:16'),
(56, 3, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-18 04:10:04', '2023-01-18 04:10:04'),
(57, 5, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-18 04:11:11', '2023-01-18 04:11:11'),
(58, 8, '::1', '60-A5-E2-9F-62-16   Media disconnected', '2023-01-18 04:27:26', '2023-01-18 04:27:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance_details`
--
ALTER TABLE `attendance_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inter_missions`
--
ALTER TABLE `inter_missions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_requests`
--
ALTER TABLE `leave_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance_details`
--
ALTER TABLE `attendance_details`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inter_missions`
--
ALTER TABLE `inter_missions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `leave_requests`
--
ALTER TABLE `leave_requests`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
