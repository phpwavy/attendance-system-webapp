<?php
    use Carbon\Carbon;
    function get_formatted_time($time_para){
        return date('h:i A', strtotime($time_para));
    }
    function startAndEndMonth($date){
        $start_month = Carbon::now()->parse($date)->startOfMonth();
        $end_month = Carbon::now()->parse($date)->endOfMonth();
        $start=Carbon::parse($start_month)->format("Y-m-d");
        $end=Carbon::parse($end_month)->format("Y-m-d");
        $return_Data=[
            'start_month'=>$start,
            'end_month'=>$end
        ];
        return $return_Data;
    }

?>
