<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'users_id',
        'leave_type',
        'start_date',
        'end_date',
        'which_half',
        'start_time',
        'end_time',
        'reason',
        'status',
        'created_at',
        'updated_at',
        'msg',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');
    }
}