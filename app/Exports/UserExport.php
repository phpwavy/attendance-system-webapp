<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;


class UserExport implements FromCollection , WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $to_date;
    public $from_date;

    public function __construct($to_date, $from_date){
        $this->to_date = $to_date;
        $this->from_date = $from_date;
    }

    public function collection()
    {
        $users = User::latest()->select(['id', 'name', 'email', 'role', 'status']);
        $from_date = $this->from_date;
        $to_date = $this->to_date;

        if(!empty($from_date)){
            $users = $users->where('created_at', '>=', $from_date . " 00:00");
        }

        if(!empty($to_date)){
            $users = $users->where('created_at', '<=', $to_date . " 23:59");
        }

        // dd($users);
        $users =  $users->get();



        return $users;
    }
    public function headings(): array
    {
        return [
            '#',
            'name',
            'email',
            'role (1 => "USER", 2 => "ADMIN")',
            'status'
        ];
    }

}
