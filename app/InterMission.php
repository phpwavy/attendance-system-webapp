<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterMission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'break_in',
        'break_out',
        'attendance_details_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AttendanceDeatil()
    {
        return $this->belongsTo('App\AttendanceDetail', 'attendance_details_id', 'id');
    }
}