<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AttendanceDetail;
use App\Models\User;
use Auth;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use DB;

class AttendanceController extends Controller
{
    public $current_date;
    // set current Time
    public function __construct(){
        $this->current_date = date('Y-m-d');
    }
    public function today_attendance_detail(){
        $today_attendance_detail = AttendanceDetail::where('users_id', '=', Auth::id())->whereDate('created_at','=',$this->current_date)->first();
        return $today_attendance_detail;
    }
    /*
    * name: punch_in
    * description :-
    */
    // Punch in - will check  is user already punched in or not if already punch in return message and if not punched in then it will make entry of user in the db
    public function punch_in(){
        $today_attendance_detail = $this->today_attendance_detail();
        $attendence_detail = new AttendanceDetail();
        if(!empty($today_attendance_detail)){
            $response_data = [
                'message'=>'Your session already started',
                'status' => "pia",
            ];
        }else{
            $attendence_detail->office_in = date('Y-m-d H:i:s');
            $attendence_detail->users_id = Auth::id();
            $attendence_detail->save();
            $time = date("h:i A",strtotime($attendence_detail->office_in));
            // Session::flash('message', 'This is a message!');
            $response_data = [
                'punch_in' => $time,
                'message'=>'Punch In Successfully',
                'status' => 'ps',
                'today_attendance_detail' => AttendanceDetail::find($attendence_detail->id),
            ];
        }
        return $response_data;
    }
    // punch out -
    // 1. check the today attendance row exists or not
    // 2. if attendance row exists
    //   2 (i) then check  (office_in have data and lunch in and office out is empty) or (office_in and lunch_in and lunch out have punched and office_out is empty)
    public function punch_out(){
        $today_attendance_detail = $this->today_attendance_detail();
        if(!empty($today_attendance_detail)){

            if( ( $today_attendance_detail->office_in != '' && $today_attendance_detail->lunch_in == '' && $today_attendance_detail->office_out=='') || ($today_attendance_detail->office_in != '' && $today_attendance_detail->lunch_in != '' && $today_attendance_detail->lunch_out !='' && $today_attendance_detail->office_out=='' ) ){
                $today_attendance_detail->office_out = date('Y-m-d H:i:s');
                $today_attendance_detail->save();
                $time = date("h:i A",strtotime($today_attendance_detail->office_out));
                $response_data=[
                    'punch_out' => $time,
                    'message' => 'Punch Out Successfully',
                    'status' => 'pos',
                    'today_attendance_detail' => $today_attendance_detail
                ];

            }else{
                $response_data=[
                    'message'=>'please contact with admin',
                    'status' => 'ca'
                ];

            }
        }else{
            $response_data=[
                'message'=>'Please Start Your Session',
                'status' => 'ss'
            ];
        }
        return $response_data;
    }
    public function lunch_in(){
        $today_attendance_detail = $this->today_attendance_detail();
        if( !empty($today_attendance_detail) ){
            //
            if($today_attendance_detail->office_in != '' && $today_attendance_detail->office_out == '' && $today_attendance_detail->lunch_in == '' && $today_attendance_detail->lunch_out  == ''){
                $today_attendance_detail->lunch_in = date('Y-m-d H:i:s');
                $today_attendance_detail->save();
                $time = date("h:i A",strtotime($today_attendance_detail->lunch_in));
                $response_data = [
                    'lunch_in'=> $time,
                    'message'=>'lunch In Successfully',
                    'status' => 'lis',
                    'today_attendance_detail' => $today_attendance_detail
                ];
            } else {
                $response_data=[
                    'message'=>'please contact with admin',
                    'status' => 'ca'
                ];
            }

        }else{
            $response_data=[
                'message'=>'Please Start Your Session',
                'status' => 'ss'
            ];
        }

        return $response_data;
    }
    public function lunch_out(){
        $today_attendance_detail = $this->today_attendance_detail();
        if( !empty($today_attendance_detail) ){
            if($today_attendance_detail->office_in != "" && $today_attendance_detail->lunch_in != "" && $today_attendance_detail->lunch_out == "" && $today_attendance_detail->office_out == ""){
                $today_attendance_detail->lunch_out = date('Y-m-d H:i:s');
                $today_attendance_detail->save();
                $time = date("h:i A",strtotime($today_attendance_detail->lunch_out));
                $response_data=[
                    'lunch_out'=>$time,
                    'message'=>'Lunch Out Successfully',
                    'status' => 'los',
                    'today_attendance_detail' => $today_attendance_detail
                ];
            }else{
                $response_data = [
                    'message'=>'please contact with admin',
                    'status' => 'ca'
                ];
            }
        }else{
            $response_data=[
                'message'=>'Please Start Your Session',
                'status' => 'ss'
            ];
        }

        return $response_data;

    }
    public function activity(Request $request){
        if (Auth::check()) {
            $attendence_detail = new AttendanceDetail();
            $user_id = Auth::User()->id;

            // Response status means : -> ("pia" => "Punch In already started", "pis" => "Punch in successfully", "pos" => "Punch out successfully",  "ss" => "Session Start", ca => "contact admin", "los => "Lunch Out Successfully", lis => "Lunch In Successfully")
            $activity_type = $request->activity;

            if($activity_type == 'punch_in'){
                $response=$this->punch_in();
                return response()->json($response);
            }else if($activity_type == "punch_out"){
                $response = $this->punch_out();
                return response()->json($response);
            }else if($activity_type == "lunch_in"){
                $response = $this->lunch_in();
                return response()->json($response);
            }else if($activity_type == "lunch_out"){
                $response = $this->lunch_out();
                return response()->json($response);
            }

        }
    }
    public function create_attendance(){
        return view('admin.attendance.add');
    }
    public function all_attendance(Request $request){
        $users=DB::select('SELECT *,users.id as user_id FROM users LEFT JOIN attendance_details ON (attendance_details.users_id = users.id) WHERE (office_in >="'.$this->current_date.' 00:00" AND office_in <="'.$this->current_date.' 23:59")  OR attendance_details.users_id IS NULL;');
        // $users = User::where('status','=',1)
        // ->where('role','!=',2)
        // ->latest()
        // ->get();
        // foreach($users as $user){
        //     $detail=AttendanceDetail::where('users_id',$user->id)->where('office_in','>=',$this->current_date.' 00:00')->where('office_in','<=',$this->current_date.' 23:59')->first();
        //     if(!empty($detail)){
        //         $user['attendance'].=$detail;
        //     }
        // }
        // $users = $users->AttendanceDetail()->where(function (Builder $query) {
        //     return $query->where('office_in','>=',$this->current_date.' 00:00');
        // });
        // dd($users);
        //$usersAttendanceDetail=AttendanceDetail::latest();
        // if(!empty($request->from)){
        //     $from=date('Y-m-d H:i:s',strtotime($request->from));
        //     $usersAttendanceDetail=$usersAttendanceDetail->where('office_in','>=',$from);
        // }
        // if(!empty($request->to)){
        //     $to=date('Y-m-d H:i:s',strtotime($request->to));
        //     $usersAttendanceDetail = $usersAttendanceDetail->where('office_in','<=',$to);
        // }
        // if(!empty($request->id)){
        //     $usersAttendanceDetail=$usersAttendanceDetail->where('users_id',$request->id);
        // }
        // if(empty($_GET)){
        //     $usersAttendanceDetail=$usersAttendanceDetail->where('office_in','>=',$this->current_date.' 00:00');
        // }
        //$usersAttendanceDetail = $usersAttendanceDetail->get();
        return view('admin.attendance.index',compact('users'));
    }
    public function edit_attendance($id){
        $userAttendanceDetail = AttendanceDetail::find($id);
        return view('admin.attendance.edit',compact('userAttendanceDetail'));
    }
    public function store_attendance(Request $request){
        $request->validate([
            'users_id' => 'required',
            'office_in' => 'required',
        ]);
        $attendence_detail = AttendanceDetail::where('users_id', $request->users_id)->where('office_in', $request->office_in)->first();
        if(!empty($attendence_detail)){
            $attendence_detail->update($request->all());
        }else{
            AttendanceDetail::create($request->all());
        }

        return redirect()->back()->with('status', 'created successfully');
    }
    public function destroy_attendance($id){
      AttendanceDetail::find($id)->delete();
      return redirect()->route('attendances.index')->with('status','Delete successfully.');
    }
}
