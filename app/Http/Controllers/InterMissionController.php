<?php

namespace App\Http\Controllers;

use App\InterMission;
use Illuminate\Http\Request;

class InterMissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InterMission  $interMission
     * @return \Illuminate\Http\Response
     */
    public function show(InterMission $interMission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InterMission  $interMission
     * @return \Illuminate\Http\Response
     */
    public function edit(InterMission $interMission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InterMission  $interMission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InterMission $interMission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InterMission  $interMission
     * @return \Illuminate\Http\Response
     */
    public function destroy(InterMission $interMission)
    {
        //
    }
}
