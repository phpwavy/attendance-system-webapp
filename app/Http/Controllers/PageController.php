<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\holiday;
use App\AttendanceDetail;
use Auth;
use Carbon\Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;


class PageController extends Controller
{
    public $start_month;
    public $end_month;
    public $Current_year;
    //
    public function __construct(){
        $this->start_month = Carbon::now()->startOfMonth();
        $this->end_month = Carbon::now()->endOfMonth();
        $this->Current_year = Carbon::now()->format('Y');
    }

    public function dashboard(){
        if(Auth::User()->role == 2){
             $admin_dashboard = $this->admin_dashboard();
             return view($admin_dashboard['view_name'], $admin_dashboard['data']);
         }else{
           $user_dashboard = $this->user_dashboard();

           return view($user_dashboard['view_name'], $user_dashboard['data']);
         }
     }

     public function admin_dashboard(){
        // Total Users
        $total_users = User::get()->count();
        // Total Inactive Users
        $total_inactive_users = User::where('status',2)->get()->count();
        // Total Active Users
        $total_active_users = User::where('status',1)->get()->count();
        // Current year holidays
        $holidays = holiday::whereYear('event_date',$this->Current_year)->get();

        $return_data = [
            'total_users' => $total_users,
            'total_active_users' => $total_active_users,
            'total_inactive_users' => $total_inactive_users,
            'holidays'=>$holidays
        ];

        $view_name  = "admin.admin-dashboard";
        return (['data' => $return_data, 'view_name' =>  $view_name]);
    }

     public function get_current_month_holiday(){
        $holidays = holiday::latest()
        ->where('event_date', '<=',  $this->end_month)
        ->where('event_date', '>=', $this->start_month)
        ->get();
        return $holidays;
     }


     public function user_dashboard(){
        $user_id = Auth::User()->id;
        // To get holidays of current month
        $holidays = $this->get_current_month_holiday();
        $current_date = date('Y-m-d');
        // Attendance Detail of Today
        $attendance_detail = AttendanceDetail::where( 'users_id', '=', $user_id )->whereDate( 'office_in','=',$current_date )->first();
        // Total Attendance of Current Month
        $attendance_count = AttendanceDetail::where( 'office_in', '>=',  $this->start_month )->where( 'office_in', '<=', $this->end_month )->count();

        // $today_day_in_month = Carbon::now()->month(date("m"))->daysInMonth;

        // dd($total_day_from_now);
        $total_absent = date("d") - $attendance_count;

        // Counter Time of Watch
        $counter_time = array(
            'hours' => 0,
            'minutes' => 0,
            'seconds' => 0,
        );

        if( !empty($attendance_detail) ){
            if( !empty($attendance_detail->office_in) && empty($attendance_detail->office_out) ){
               $current_date_time =  new DateTime();
               $office_in = new DateTime($attendance_detail->office_in);
               $interval = $current_date_time->diff($office_in);
               $counter_time['hours'] = $interval->h;
               $counter_time['minutes'] = $interval->i;
               $counter_time['seconds'] = $interval->s;
            }
        }

        // $days = $this->number_of_working_days($start_month,$end_month);

        $return_data = [
            'holidays' => $holidays,
            'attendance_detail' => $attendance_detail,
            'attendance_count' => $attendance_count,
            'counter_time' => $counter_time,
            'total_absent' => $total_absent,
        ];

        $view_name  = "users.dashboard";
        return (['data' => $return_data, 'view_name' =>  $view_name]);

    }

    // function number_of_working_days($from, $to) {
    //     $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
    //     $holidayDays = ['*-12-25', '*-01-01', '2022-12-23']; # variable and fixed holidays

    //     $from = new DateTime($from);
    //     $to = new DateTime($to);
    //     $to->modify('+1 day');
    //     $interval = new DateInterval('P1D');
    //     $periods = new DatePeriod($from, $interval, $to);

    //     $days = 0;
    //     foreach ($periods as $period) {
    //         if (!in_array($period->format('N'), $workingDays)) continue;
    //         if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
    //         if (in_array($period->format('*-m-d'), $holidayDays)) continue;
    //         $days++;
    //     }
    //     return $days;
    // }


    public function downloadExcel()
    {
        $data = User::get()->toArray();
        return Excel::download(new UserExport('2022-12-12', '2022-11-01'), 'users.xlsx');
    }
}
