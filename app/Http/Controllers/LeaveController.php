<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeaveRequest;
use Auth;
use Validator;
use Mail;
use App\Models\User;

class LeaveController extends Controller
{
    //
    public function apply_leave_request(){
        return view('users.leave-request.add');
    }


    public function index(){
        $leave_requests = LeaveRequest::where('users_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        return view('users.leave-request.index', compact('leave_requests'));
    }

    public function all_leave_request(){
        $leave_requests = LeaveRequest::latest()->orderBy('id', 'desc')->get();
        return view('admin.leave-request.index', compact('leave_requests'));
    }

    public function store_leave_request(Request $request){
        $validator = Validator::make($request->all(), [
            'leave_type' => 'required',
            'reason' => 'required',
            'start_date' => ($request->leave_type == "Full") ? 'required': 'nullable',
            'end_date' => ($request->leave_type == "Full") ? 'required': 'nullable',
            'on_half_day_date' => ($request->leave_type == "Half") ? "required" : "nullable",
            'which_half' => ($request->leave_type == "Half") ? "required" : "nullable",
            'on_short_day_date' => ($request->leave_type == "Short") ? "required" : "nullable",
            'start_time' => ($request->leave_type == "Short") ? "required": "nullable",
            'end_time' => ($request->leave_type == "Short") ? "required" : "nullable",
        ], [
            'leave_type.required' => "Leave Type is required",
            'reason.required' => "Reason is required",
            "start_date.required" => "Start Date is required",
            "end_date.required" => "End Date is required",
            "on_half_day_date.required" => "Date is required",
            "which_half.required" => "Which Half is required",
            "on_short_day_date.required" => "Date is required",
            "start_time.required" => "Start Time is required",
            "end_time.required" => "End Time is required",
        ]);

        $errors = [];

        if($validator->passes()){
            if($request->leave_type == "Full"){
                $data = array(
                    "leave_type" => $request->leave_type,
                    "reason" => $request->reason,
                    "start_date" => $request->start_date,
                    "end_date" => $request->end_date,
                    "users_id" => Auth::user()->id,
                );
            }else if($request->leave_type == "Half"){
                $data = array(
                    "leave_type" => $request->leave_type,
                    "reason" => $request->reason,
                    "start_date" => $request->on_half_day_date,
                    "which_half" => $request->which_half,
                    "users_id" => Auth::user()->id,
                );
            }else if($request->leave_type == "Short"){
                $data = array(
                    "leave_type" => $request->leave_type,
                    "reason" => $request->reason,
                    "start_date" => $request->on_short_day_date,
                    "start_time" => $request->start_time,
                    "end_time" => $request->end_time,
                    "users_id" => Auth::user()->id,
                );
            }else{
                $data = [];
            }
        }else{
            if($validator->errors()){
                $errors = [];
                foreach($validator->errors()->all() as $error){
                    array_push($errors, $error);
                }
            }
        }


        $res = [];

        if(!empty($data)){
            LeaveRequest::create($data);
            $res = array('status' => 'success', 'msg' => "Applied Successfully", 'errors' => []);
            $subject = $data['start_date'] . " | " . $data['leave_type'];

            $email_data = array(
                'leave' => array('leave_type' => $data['leave_type'], 'reason' => $data['reason'], 'start_date' => $data['start_date'], 'end_date' => $request->end_date, 'start_time' => $request->start_time, 'end_time' => $request->end_time,  'which_half' => $request->which_half, 'current_date' => date('Y-m-d')),
                'user' => array('name' => Auth::user()->name, 'email' => Auth::user()->email),
            );

            $this->sent_mail("email-templates.leave-report-to-admin", $email_data, "admin@gmail.com", $subject);
        }else{
            $res = array('status' => 'error', 'msg' => 'something went wrong', 'errors' => $errors);
        }


        return response()->json($res);

    }

    public function cancel_request($id){
        $leave_request = LeaveRequest::findOrFail($id);
        if(!empty($leave_request->users_id)){
            if($leave_request->users_id == Auth::user()->id){
                if($leave_request->status == 1){
                    $leave_request->update(['status' => 5]);
                    return redirect()->back()->with('status', 'Cancelled Successfully');
                }else{
                    return redirect()->back()->with('error', 'Only pending status leave request can cancelled. if you still want to cancel please contact to admin.');
                }
            }else{
                return redirect()->back()->with('error', 'something went wrong');
            }
        }else{
            return redirect()->back()->with('error', 'Invalid Id');
        }
    }


    public function change_request_status($id){
        $leave_request = LeaveRequest::findOrFail($id);
        return view('admin.leave-request.edit', compact('leave_request'));
    }


    public function udpate_request_status(Request $request, $id){
      $leave_request = LeaveRequest::findOrFail($id);

      $old_msg = $leave_request->message;
      $old_status = $leave_request->status;
    //   $change = false;
      $status_msg = "";
      if($old_status != 5){
        if(!empty($request->message) || !empty($request->status)){

            // if($old_msg != $request->message){
            //     $change = true;
            // }

            // if($request->status == $old_status){
            //     $change = true;
            // }

            // if($change){
                $leave_request->update(['status' => $request->status, 'msg' => $request->message]);
                $status_msg = "Updated Successfully";
                $user = User::where('id', $leave_request->users_id)->first();
                if(!empty($user)){
                    if(!empty($user->email)){

                        $leave_request_data['leave'] = $leave_request;

                        // 1 => Pending, 2 => Processing, 3 => Reject, 4 => Approved
                        if($leave_request->status == 1){
                            $leave_status = "PENDING";
                        }else if($leave_request->status == 2){
                            $leave_status = "PROCESSING";
                        }else if($leave_request->status == 3){
                            $leave_status = "REJECTED";
                        }else if($leave_request->status == 4){
                            $leave_status = "APPROVED";
                        }else{
                            $leave_status = "";
                        }
                        $leave_request_data['status'] = $leave_status;
                        $subject = $leave_status . " | " . $leave_request->start_date . " | " . $leave_request->leave_type;
                        $this->sent_mail("email-templates.leave-status-to-user", $leave_request_data, $user->email, $subject);
                    }
                }

            // }
        }
        }else{
            $status_msg = "Already Updated";
        }
      return redirect()->route('all_leave_request')->with('status', $status_msg);
    }


    public function sent_mail($email_template, $data, $to, $subject){

        Mail::send($email_template, $data, function($message) use ($to, $subject){
            $message->to($to, $to)->subject($subject);
            $message->from('test@gmail.com','Testing');
        });
        return true;

    }
}





