<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\UserLog;

class UserLogController extends Controller
{
    //
    public function show($id, Request $request){

        $user = User::findOrFail($id);

        $log_query = UserLog::latest()->where('users_id', $user->id);

        if(!empty($request->from)){
           $log_query  = $log_query->where('created_at', '>=', $request->from . " 00:00");
        }


        if(!empty($request->to)){
            $log_query  = $log_query->where('created_at', '<=', $request->to . " 23:59");
        }

        $all_logs = $log_query->get();
        return view('admin.logs.index', compact('all_logs'));
    }
}
