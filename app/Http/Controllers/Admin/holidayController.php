<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\holiday;
use Illuminate\Support\Facades\DB;


class holidayController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $holidays = Holiday::latest();
        if(!empty($request->from)){
           $holidays = $holidays->where('event_date', '>=', $request->from);
        }
        if(!empty($request->to)){
            $holidays = $holidays->where('event_date', '<=', $request->to);
        }
        $holidays = $holidays->get();
        return view('admin.holidays.index',compact('holidays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {

      return view('admin.holidays.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'event_date' => 'required',
            'event' => 'required',
       ]);

       $input = $request->all();
       $input['status'] = isset($request->is_optional) ? "Yes" : "No";
       $event = Holiday::create($input);
       return redirect()->route('holidays.index')->with('status', 'New Holiday Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $holiday = Holiday::findorFail($id);
        return view('admin.holidays.edit',compact('holiday'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $request->validate([
        'event_date'=>'required',
        'event'=>'required',
     ]);
     $data = Holiday::findorFail($id);
     $input = $request->all();
     $input['status'] = isset($request->is_optional) ? "Yes" : "No";
     $data->update($input);
     return redirect()->back()->with('status','Holiday Updated successfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Holiday::findOrFail($id)->delete($id);
        return redirect()->route('holidays.index')->with('status', 'Deleted Successfully !');
    }
}
