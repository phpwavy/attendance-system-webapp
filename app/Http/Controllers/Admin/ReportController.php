<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AttendanceDetail;
use App\userLog;
use App\Models\User;
use Auth;
use Carbon\Carbon;

class ReportController extends Controller
{
    //

    public function report($id){
       $reports =  AttendanceDetail::latest()->where('users_id', $id)->get();
       return view('admin.usersreport.index', compact('reports'));
    }

    public function all_report(Request $request){
        if(!empty($request)){
            if($request->month!="" && $request->id!=""){
               $reports =  AttendanceDetail::latest();
               $start_month=Carbon::parse($request->month)->format('Y-m-d H:i:s');
               $end_month=Carbon::now()->parse($start_month)->endOfMonth();
               $reports= $reports->where('office_in','>=',$start_month)->where('office_in','<=',$end_month)->where('users_id',$request->id);
               $reports= $reports->get();
            }else{
              $reports=[];
            }
        }
        return view('admin.usersreport.all-detail', compact('reports'));
        // if(!empty($request->from)){
        //     $from=date('Y-m-d H:i:s',strtotime($request->from));
        //     $reports= $reports->where('office_in','>=',$from);
        // }
        // if(!empty($request->to)){
        //     $to=date('Y-m-d H:i:s',strtotime($request->to));
        //     $reports= $reports->where('office_in','<=',$to);
        // }
        // if(!empty($request->year)){
        //     if($request->year == 'next_month'){
        //         $next_month=carbon::now()->addMonth();
        //         $date=startAndEndMonth($next_month);
        //         $reports= $reports->where('office_in','>=',$date['start_month'].' 00:00')->where('office_in','<=',$date['end_month'].' 23:59');
        //     }elseif($request->year == 'prev_month'){
        //         $prev_month=carbon::now()->subMonths();
        //         $date=startAndEndMonth($prev_month);
        //         $reports= $reports->where('office_in','>=',$date['start_month'].' 00:00')->where('office_in','<=',$date['end_month'].' 23:59');
        //     }elseif($request->year == 'next_year'){
        //         $next_year = Carbon::now()->addYear()->format('Y');
        //         $reports= $reports->where('office_in','>=',$next_year.'-01-01'.' 00:00')->where('office_in','<=',$next_year.'-12-31'.' 23:59');
        //     }elseif($request->year == 'prev_year'){
        //         $prev_year = Carbon::now()->subYear()->format('Y');
        //         $reports= $reports->where('office_in','>=',$prev_year.'-01-01'.' 00:00')->where('office_in','<=',$prev_year.'-12-31'.' 23:59');
        //     }
        // }

    }

    public function show_log_report(){
            $log_query = UserLog::latest();
            if(!empty($request->from)){
               $log_query  = $log_query->where('created_at', '>=', $request->from . " 00:00");
            }
            if(!empty($request->to)){
                $log_query  = $log_query->where('created_at', '<=', $request->to . " 23:59");
            }
            $all_logs = $log_query->get();
            return view('admin.logs.index', compact('all_logs'));
    }

    public function show_user_log(Request $request){
        $log_query = UserLog::latest()->where('users_id', Auth::user()->id);
        if(!empty($request->from)){
           $log_query  = $log_query->where('created_at', '>=', $request->from . " 00:00");
        }
        if(!empty($request->to)){
            $log_query  = $log_query->where('created_at', '<=', $request->to . " 23:59");
        }
        $all_logs = $log_query->get();
        return view('users.report.log-report', compact('all_logs'));
    }

    public function show_user_report(){
        $reports =  AttendanceDetail::latest()->where('users_id', Auth::user()->id)->get();
        return view('users.report.attendance-detail', compact('reports'));
    }




}
