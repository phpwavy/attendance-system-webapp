<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::latest()->where('role', '!=', 2)->get();
        return view('admin.users.manage-users', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.users.add-users');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'phone' => 'required',
            'status' => 'required',
            'address' => 'required',
            'profile_image' => 'required|mimes:jpeg,png,jpg,gif',
            'emergency_contact_name' => 'required',
            'emergency_contact_relation' => 'required',
            'emergency_contact_phone' => 'required',
            'roles_id' => 'required',
            'country' =>'required',
            'city' => 'required',
            'state' => 'required',
        ],

        [
            'name.required' => "Name is required",
            'email.required'  => 'Email is required',
            'password.required' => 'Password is required',
            'password.min' => 'Password must atleast 6 characters',
            'email.email' => 'Invalid email',
            'email.unique' => 'Email already exists',
            'phone.required' => 'Phone is required',
            'status.required' => 'Status is required',
            'address.required' => 'Address is required',
            'profile_image.required' => 'Profile image is required',
            'profile_image.mimes' => 'Only jpeg, png, gif are allowed',
            'emergency_contact_name.required' => 'Emergency contact name is required',
            'emergency_contact_relation.required' => 'Emergency contact relation is required',
            'emergency_contact_phone.required' => 'Emergency contact phone is required',
            'roles_id.required' => 'Designation is required',
            'country' =>'Country is required',
            'city' => 'City is required',
            'state' => 'State is required',
        ]

        );

        $input = $request->all();

        $input['password'] = Hash::make($request->password);

        $user = User::create($input);


        if($request->has('profile_image')){
            $profile_image = $request->file('profile_image');
            $ex = $request->file('profile_image')->getClientOriginalExtension();
            $file_name = time() . rand(1,10) . "." . $ex;
            $path = public_path("/uploads/profile/");
            $profile_image->move($path, $file_name);
            $input['profile_image'] = "uploads/profile/" . $file_name;
        }

        $user->UserDetail()->create($input);

        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::findOrFail($id);
        return view('admin.users.show-user', compact('user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $user = User::findOrFail($id);
        return view('admin.users.edit-users', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::findOrFail($id);



        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $id,
            'password' => !empty($request->password) ? 'min:6': 'nullable',
            'phone' => 'required',
            'status' => 'required',
            'address' => 'required',
            'profile_image' => 'mimes:jpeg,png,jpg,gif',
            'emergency_contact_name' => 'required',
            'emergency_contact_relation' => 'required',
            'emergency_contact_phone' => 'required',
            'join_date' => 'required',
            'roles_id' => 'required',
        ],


        [
            'name.required' => "Name is required",
            'email.required'  => 'Email is required',
            'password.min' => 'Password must atleast 6 characters',
            'email.email' => 'Invalid email',
            'email.unique' => 'Email already exists',
            'phone.required' => 'Phone is required',
            'status.required' => 'Status is required',
            'address.required' => 'Address is required',
            'profile_image.mimes' => 'Only jpeg, png, gif are allowed',
            'emergency_contact_name.required' => 'Emergency contact name is required',
            'emergency_contact_relation.required' => 'Emergency contact relation is required',
            'emergency_contact_phone.required' => 'Emergency contact phone is required',
            'join_date.required' => 'Date of Joining is required',
            'roles_id.required' => 'Designation is required',
        ]);


        $input = $request->all();

        if(!empty($input['password'])){
            $input['password'] = Hash::make($request->password);
        }else{
            unset($input['password']);
        }

        DB::beginTransaction();

        $user_updated = $user->update($input);

        $userDetail['phone'] = $request->phone;
        $userDetail['address'] = $request->address;

        $userDetail['emergency_contact_name'] = $request->emergency_contact_name;
        $userDetail['emergency_contact_relation'] = $request->emergency_contact_relation;
        $userDetail['emergency_contact_phone'] = $request->emergency_contact_phone;
        $userDetail['join_date'] = $request->join_date;

        if($request->has('profile_image')){

            $old_img_name = $user->UserDetail->profile_image;

            $profile_image = $request->file('profile_image');
            $ex = $request->file('profile_image')->getClientOriginalExtension();
            $file_name = time() . rand(1,10) . "." . $ex;
            $path = public_path("/uploads/profile/");
            $profile_image->move($path, $file_name);
            $userDetail['profile_image'] = "uploads/profile/" . $file_name;
        }

        $user_detail_updated = $user->UserDetail()->update($userDetail);

        if(!$user_updated && !$user_detail_updated){
            DB::rollback();
            return redirect()->back()->with('status', 'Something went wrong');
        }else{
            DB::commit();
            if($request->has('profile_image')){
                if(File::exists(public_path($old_img_name))){
                    File::delete(public_path($old_img_name));
                }
            }
        }

        return redirect()->back()->with('status', 'updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::findOrFail($id)->delete($id);
        return redirect()->back()->with('status', 'Deleted Successfully');
    }

    public function only_trashed_user(){
        $users = User::onlyTrashed()->get();
        return view('admin.users.trashed-users', compact('users'));
    }

    public function recover_user($id){
        $user = User::onlyTrashed()->where('id', $id)->first();
        $user->restore();
        return redirect()->route('users.index')->with('status', 'restored successfully..');
    }

    public function remove_user_permanently(Request $request){
        DB::beginTransaction();
        $user_detail_delete = UserDetail::where('users_id', $request->user_id)->delete();
        $user_delete = User::onlyTrashed()->where('id', $request->user_id)->forceDelete();
        if(!$user_delete && !$user_detail_delete){
            DB::rollback();
            return redirect()->back()->with('error', 'Something went wrong');
        }else{
            DB::commit();
            return redirect()->back()->with('status', 'Deleted permanently');
        }
    }
}
