<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class holiday extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'event',
        'event_date',
        'status',
    ];
}
