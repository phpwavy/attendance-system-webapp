<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'users_id',
        'address',
        'phone',
        'created_at',
        'updated_at',
        'profile_image',
        'emergency_contact_name',
        'emergency_contact_relation',
        'emergency_contact_phone',
        'join_date',
        'country',
        'city',
        'state',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');
    }
}