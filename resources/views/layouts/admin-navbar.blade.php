<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-users"></i>
        <p>
            Users
            <i class="right fas fa-angle-left"></i>

        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('users.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>All Users</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('only_trashed_user') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Trashed Users</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('users.create') }} " class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Users</p>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-list-alt"></i>
        <p>
            Holiday List
            <i class="right fas fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('holidays.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>All Holiday</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('holidays.create') }} " class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Holiday</p>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-user-tag"></i>
        <p>
            Roles
            <i class="right fas fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('roles.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>All Roles</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('roles.create') }} " class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Roles</p>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-user-edit"></i>
        <p>
            Today Attendance
            <i class="right fas fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('attendances.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Today Attendance</p>
            </a>
        </li>

        {{-- <li class="nav-item">
            <a href="{{ route('attendance.create') }} " class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Today Attendance</p>
            </a>
        </li> --}}
    </ul>
</li>
<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-calendar-day"></i>
        <p>
            Leave Requests
            <i class="right fas fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('all_leave_request') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>All leave Request</p>
            </a>
        </li>
    </ul>
</li>


<li class="nav-item">
    <a href="{{ route('all_user_report') }}" class="nav-link">
        <i class="nav-icon fas fa-clipboard-check"></i>
        <p>
            Reports
        </p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('log_report') }}" class="nav-link">
        <i class="nav-icon fas fa-clipboard-check"></i>
        <p>
            Log Reports
        </p>
    </a>
</li>
