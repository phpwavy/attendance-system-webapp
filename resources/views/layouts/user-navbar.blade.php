<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-calendar-day"></i>
        <p>
            Apply For Leave
            <i class="right fas fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('leave-applied') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>All Applied Leaves</p>
            </a>
        </li>
    </ul>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('apply_leave_request') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Apply For Leaves</p>
            </a>
        </li>
    </ul>
</li>


<li class="nav-item">
    <a href="{{ route('show_user_report') }}" class="nav-link">
        <i class="nav-icon fas fa-clipboard-check"></i>
        <p>
            Reports
        </p>
    </a>
</li>



<li class="nav-item">
    <a href="{{ route('show_user_log') }}" class="nav-link">
        <i class="nav-icon fas fa-clipboard-check"></i>
        <p>
            Log Reports
        </p>
    </a>
</li>
