<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>

<body>
    <h4>{{ $user['name'] }} applied for the leave.</h4>
    <table border="1">
        <tr>
            <th>Leave Type</th>
            <td>{{ $leave['leave_type'] }}</td>
        </tr>

        @if ($leave['leave_type'] == 'Full')
            <tr>
                <th>Start Date</th>
                <td>{{ $leave['start_date'] }}</td>
            </tr>

            <tr>
                <th>End Date</th>
                <td>{{ $leave['end_date'] }}</td>
            </tr>
        @elseif($leave['leave_type'] == 'Half')
            <tr>
                <th>Date</th>
                <td>{{ $leave['start_date'] }}</td>
            </tr>

            <tr>
                <th>Which Half</th>
                <td>{{ $leave['which_half'] }}</td>
            </tr>
        @elseif($leave['leave_type'] == 'Short')
            <tr>
                <th>Date</th>
                <td>{{ $leave['start_date'] }}</td>
            </tr>

            <tr>
                <th>Start Time</th>
                <td>{{ $leave['start_time'] }}</td>
            </tr>

            <tr>
                <th>End Time</th>
                <td>{{ $leave['end_time'] }}</td>
            </tr>
        @endif




        <tr>
            <th>Reason</th>
            <td>{{ $leave['reason'] }}</td>
        </tr>

        <tr>
            <th>Apply At</th>
            <td>{{ $leave['current_date'] }}</td>
        </tr>


    </table>
</body>

</html>
