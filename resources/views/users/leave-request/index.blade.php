@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Manage Leave Request')

@section('breadcrumb-title', 'Manage Leave Request')

@section('main-content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-custom-danger alert-dismissible fade show" role="alert">
                    <strong>Please check again.</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session()->has('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('apply_leave_request') }}" class="btn btn-info btn-sm mb-3">Apply Leave</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Manage Leave</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Leave Type</th>
                                        <th>Date</th>
                                        <th>End Date</th>
                                        {{-- <th>Date</th> --}}
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Reason</th>
                                        <th>Status</th>
                                        <th>Cancel</th>
                                        <th>Apply At</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    @foreach ($leave_requests as $key => $leave)
                                        <tr>
                                            <td>{{ $loop->iteration }}.</td>
                                            <td>{{ $leave->leave_type }}</td>
                                            <td>{{ $leave->start_date }}</td>
                                            <td>
                                                @if ($leave->leave_type == 'Full')
                                                    {{ $leave->end_date }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            {{-- <td>12-10-2022</td> --}}
                                            <td>
                                                @if ($leave->leave_type == 'Short')
                                                    {{ $leave->start_time }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if ($leave->leave_type == 'Short')
                                                    {{ $leave->end_time }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>{{ $leave->reason }}</td>
                                            <td>
                                                <span class="badge badge-secondary">
                                                    @if ($leave->status == '4')
                                                        Approved
                                                    @elseif ($leave->status == '2')
                                                        Processing
                                                    @elseif($leave->status == '3')
                                                        Reject
                                                    @elseif($leave->status == '5')
                                                        Cancelled
                                                    @else
                                                        Pending
                                                    @endif
                                                </span>
                                            </td>
                                            <td>
                                                @if ($leave->status == 1)
                                                    <a href="{{ route('cancel_request', ['id' => $leave->id]) }}">
                                                        Cancel
                                                    </a>
                                                @else
                                                    -
                                                @endif
                                            </td>

                                            <td>{{ $leave->created_at }}</td>
                                        </tr>
                                    @endforeach




                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
@endsection


@push('footer-script')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush
