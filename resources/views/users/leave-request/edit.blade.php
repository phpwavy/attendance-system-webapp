@extends('layouts.main')
@push('custom-css')
@endpush
@section('title', 'Edit Holiday | MyTracker')

@section('breadcrumb-title', 'Edit Holiday')

@section('main-content')
    <!-- Main content -->
    <section class="content users-form">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-custom-danger alert-dismissible fade show" role="alert">
                    <strong>Please check again.</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session()->has('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('holidays.update', ['holiday' => $holiday->id]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row justify-content-center align-items-center">
                    <!-- form start -->
                    <div class="col-md-8">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Holiday Info</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Date</label>
                                            <input type="date" class="form-control" name="event_date" id="event_date"
                                                value="{{ $holiday->event_date }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="email">Event/holiday</label>
                                            <textarea class="form-control" name="event" id="email" placeholder="Holiday For">{{ $holiday->event }} </textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="{{ $holiday->status }}"
                                                id="is_optional" name="is_optional"
                                                @if ($holiday->status == 'yes') {{ 'checked' }} @endif>
                                            <label class="form-check-label font-weight-bold" for="is_optional">
                                                Is optional
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-dark form-btn float-right">Update</button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@push('footer-script')
    <script></script>
@endpush
