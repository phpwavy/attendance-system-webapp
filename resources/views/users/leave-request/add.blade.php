@extends('layouts.main')
@push('custom-css')
    <style>
        #for-fullday,
        #for-halfday,
        #for-shortday {
            display: none;
        }

        #error-alert-div {
            display: none;
        }
    </style>
@endpush
@section('title', 'Apply Leave Request | MyTracker')

@section('breadcrumb-title', 'Apply Leave Request')

@section('main-content')
    <!-- Main content -->
    <section class="content users-form">
        <div class="container-fluid">

            <div class="alert alert-custom-danger alert-dismissible fade show" role="alert" id="error-alert-div">
                <strong>Please check again.</strong>
                <ul id="error-alert-ul">

                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            @if (session()->has('status'))
                <div class="alert alert-success alert-dismissible">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" enctype="multipart/form-data" id="apply-leave-form">
                @csrf
                <div class="row justify-content-center align-items-center">
                    <!-- form start -->
                    <div class="col-md-6 mt-5">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Apply Leave Request</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="leave_type">Leave Type</label>
                                            <select name="leave_type" id="leave_type" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="Full">Full Day</option>
                                                <option value="Short">Short Day</option>
                                                <option value="Half">Half Day</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="reason">Reason</label>
                                            <textarea name="reason" id="reason" rows="3" class="form-control"></textarea>
                                        </div>
                                    </div>


                                    <div id="for-fullday" class="col-md-12">
                                        <div class="form-group">
                                            <label for="start_date">Start Date</label>
                                            <input type="date" name="start_date" id="start_date" class="form-control"
                                                min="{{ date('Y-m-d') }}">
                                        </div>


                                        <div class="form-group">
                                            <label for="start_date">End Date</label>
                                            <input type="date" name="end_date" id="end_date" class="form-control"
                                                min="{{ date('Y-m-d') }}">
                                        </div>
                                    </div>


                                    <div id="for-halfday" class="col-md-12">
                                        <div class="form-group">
                                            <label for="on_date">Date</label>
                                            <input type="date" name="on_half_day_date" id="on_half_day_date"
                                                class="form-control" min="{{ date('Y-m-d') }}">
                                        </div>


                                        <div class="form-group">
                                            <label for="which_half">Which Half?</label>
                                            <select name="which_half" id="which_half" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="first">First</option>
                                                <option value="second">Second</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div id="for-shortday" class="col-md-12">

                                        <div class="form-group">
                                            <label for="on_short_day_date">Date</label>
                                            <input type="date" name="on_short_day_date" id="on_short_day_date"
                                                class="form-control" min="{{ date('Y-m-d') }}">
                                        </div>


                                        <div class="form-group">
                                            <label for="start_time">Start Time</label>
                                            <input type="time" name="start_time" id="start_time" class="form-control"
                                                min="10:00" max="19:00">
                                        </div>


                                        <div class="form-group">
                                            <label for="end_time">End Time</label>
                                            <input type="time" name="end_time" id="end_time" class="form-control"
                                                min="10:00" max="19:00">
                                        </div>
                                    </div>



                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-dark form-btn float-right">Submit</button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@push('footer-script')
    <script>
        $("#leave_type").on('change', () => {
            var leave_type = $("#leave_type").val();
            // console.log(leave_type);
            if (leave_type != "") {
                if (leave_type == "Full") {
                    $("#for-fullday").css('display', 'block');
                    $("#for-halfday").css('display', 'none');
                    $("#for-shortday").css('display', 'none');
                } else if (leave_type == "Short") {
                    $("#for-fullday").css('display', 'none');
                    $("#for-halfday").css('display', 'none');
                    $("#for-shortday").css('display', 'block');
                } else if (leave_type == "Half") {
                    $("#for-fullday").css('display', 'none');
                    $("#for-halfday").css('display', 'block');
                    $("#for-shortday").css('display', 'none');
                }
            } else {
                $("#for-fullday").css('display', 'none');
                $("#for-halfday").css('display', 'none');
                $("#for-shortday").css('display', 'none');
            }
        });


        $("#apply-leave-form").on('submit', function(event) {
            event.preventDefault();
            var data = $(this).serialize();
            // console.log("Data: ", data);
            let leave_type = $("#leave_type").val();
            let reason = $("#reason").val().trim();
            let error = [];
            $("#error-alert-div").css('display', 'none');
            $("#error-alert-ul").html("");
            if (leave_type != "" && reason != "") {
                if (leave_type == "Full") {
                    let start_date = $("#start_date").val().trim();
                    let end_date = $("#end_date").val().trim();
                    if (start_date == "") {
                        error_msg = "Start Date is required";
                        error.push(error_msg);
                    }

                    if (end_date == "") {
                        error_msg = "End Date is required";
                        error.push(error_msg);
                    }
                } else if (leave_type == "Short") {
                    let start_time = $("#start_time").val().trim();
                    let end_time = $("#end_time").val().trim();
                    let short_date = $("#on_short_day_date").val().trim();

                    if (start_time == "") {
                        error_msg = "Start Time is required";
                        error.push(error_msg);
                    }

                    if (end_time == "") {
                        error_msg = "End Time is required";
                        error.push(error_msg);
                    }

                    if (short_date == "") {
                        error_msg = "Date is required";
                        error.push(error_msg);
                    }



                    if (start_time != "" && end_time != "") {

                        var startTime = moment(start_time, 'HH:mm:ss');
                        var endTime = moment(end_time, 'HH:mm:ss');

                        var duration = moment.duration(endTime.diff(startTime));

                        var hours = parseInt(duration.asHours());

                        let milliseconds = duration.asMilliseconds();

                        if (milliseconds <= 0 || hours > 2) {
                            error_msg = "Invalid Start time and end time";
                            error.push(error_msg);
                        }
                    }

                } else if (leave_type == "Half") {
                    let half_day_date = $("#on_half_day_date").val().trim();
                    let which_half = $("#which_half").val().trim();

                    if (half_day_date == "") {
                        error_msg = "Date is required";
                        error.push(error_msg);
                    }

                    if (which_half == "") {
                        error_msg = "Which Half is required";
                        error.push(error_msg);
                    }

                }
            } else {
                if (leave_type == "") {
                    error_msg = "Leave Type is required";
                    error.push(error_msg);
                }

                if (reason == "") {
                    error_msg = "Reason is required";
                    error.push(error_msg);
                }
            }


            if (error.length > 0) {

                let output = ``;
                error.forEach((err) => {
                    output = output + `<li>${err}</li>`;
                });


                $("#error-alert-div").css('display', 'block');
                $("#error-alert-ul").html(output);


            } else {
                $.ajax({
                    type: "POST",
                    url: "{{ route('store-leave-request') }}",
                    data: data,
                    beforeSend: function() {

                    },
                    success: function(res) {
                        if (res.status == "error") {
                            if (res.errors.length > 0) {
                                let output = ``;
                                res.errors.forEach((err) => {
                                    output = output + `<li>${err}</li>`;
                                });
                                $("#error-alert-div").css('display', 'block');
                                $("#error-alert-ul").html(output);
                            }
                        } else if (res.status == "success") {
                            alert("Applied Successfully");
                            window.location.href = "{{ route('leave-applied') }}";
                        }
                    },
                    error: function(err) {
                        alert('Something Went Wrong');
                    }
                });
            }

        });
    </script>
@endpush
