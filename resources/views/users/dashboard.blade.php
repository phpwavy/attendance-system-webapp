@extends('layouts.main')
@push('custom-css')
    <style>
        #activityDetail,
        #office_in_msg_block,
        #office_out_msg_block,
        #lunch_in_msg_block,
        #lunch_out_msg_block,
        #no_record_msg_block {
            display: none;
        }

        .pointer-event-none {
            pointer-events: none;
        }
    </style>
@endpush
@section('title', 'Profile | MyTracker')

@section('breadcrumb-title', 'Profile')

@section('main-content')
    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{ $attendance_count }}</h3>
                            <p>Total Attendance In Month</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{ $total_absent }}</h3>
                            <p>Total Absent In Month</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>44</h3>
                            <p>User Registrations</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>65</h3>

                            <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    {{-- Activity section --}}
    <section class="content">
        <div class="container-fluid">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <div class="row justify-content-center align-items-center mt-5">
                <div class="col-lg-12 text-center">
                    <div class="timer">
                        <p class="h1 font-weight-bold">Working Hour </p>
                        <p id="watch" class="h1 font-weight-bold">00 : 00 : 00</span>
                    </div>
                </div>
                <div class="col-lg-12 text-center mt-4 ">
                    <a href="javascript:void(0)"
                        class="btn btn-outline-success btn-lg  pl-5 pr-5  pt-3 pb-3 mx-2 font-weight-bold activity {{ !empty($attendance_detail) ? (!empty($attendance_detail->office_in) ? 'pointer-event-none' : '') : '' }}"
                        id="punch_in" data-value="punch_in">
                        Punch In
                    </a>
                    <a href="javascript:void(0)"
                        class="btn btn-outline-danger btn-lg pl-5 pr-5 pt-3 pb-3 mx-2 font-weight-bold activity {{ !empty($attendance_detail) ? (!empty($attendance_detail->office_out) ? 'pointer-event-none' : '') : '' }}"
                        id="punch_out" data-value="punch_out">
                        Punch Out
                    </a>
                    <a href="javascript:void(0)"
                        class="btn btn-outline-secondary btn-lg  pl-5 pr-5 pt-3 pb-3 mx-2 font-weight-bold activity {{ !empty($attendance_detail) ? (!empty($attendance_detail->lunch_in) ? 'pointer-event-none' : '') : '' }}"
                        id="lunch_in" data-value="lunch_in">
                        Lunch In
                    </a>
                    <a href="javascript:void(0)"
                        class="btn btn-outline-warning btn-lg  pl-5 pr-5 pt-3 pb-3 mx-2 font-weight-bold activity {{ !empty($attendance_detail) ? (!empty($attendance_detail->lunch_out) ? 'pointer-event-none' : '') : '' }}"
                        id="lunch_out" data-value="lunch_out">
                        Lunch Out
                    </a>
                    {{-- <p class="btn btn-outline-info btn-lg  pl-5 pr-5 pt-3 pb-3 mx-2 font-weight-bold activity " data-value="breakin" id="breakin">Break </p> --}}
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row mt-5">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Today Activity</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" id="activityDetailLoad">
                            @if (!empty($attendance_detail))
                                @if (isset($attendance_detail->office_in) && $attendance_detail->office_in != '')
                                    <div class="alert alert-success" role="alert">
                                        {{ 'Punch In at ' . get_formatted_time($attendance_detail->office_in) }}
                                    </div>
                                @endif
                                @if (isset($attendance_detail->lunch_in) && $attendance_detail->lunch_in != '')
                                    <div class="alert alert-secondary" role="alert">
                                        {{ 'Lunch In at ' . get_formatted_time($attendance_detail->lunch_in) }}
                                    </div>
                                @endif
                                @if (isset($attendance_detail->lunch_out) && $attendance_detail->lunch_out != '')
                                    <div class="alert alert-warning" role="alert">
                                        {{ 'Lunch Out at ' . get_formatted_time($attendance_detail->lunch_out) }}
                                    </div>
                                @endif
                                @if (isset($attendance_detail->office_out) && $attendance_detail->office_out != '')
                                    <div class="alert alert-danger" role="alert">
                                        {{ 'Punch Out at ' . get_formatted_time($attendance_detail->office_out) }}
                                    </div>
                                @endif
                            @else
                                <div class="alert alert-warning" role="alert">
                                    No Record
                                </div>
                            @endif

                        </div>

                        <div class="card-body" id="activityDetail">

                            <div class="alert alert-success office_in" id="office_in_msg_block" role="alert">
                                Punch In at: <span id="office_in_time"></span>
                            </div>

                            <div class="alert alert-secondary lunch_in" id="lunch_in_msg_block" role="alert">
                                Lunch In at: <span id="lunch_in_time"></span>
                            </div>

                            <div class="alert alert-warning lunch_out" id="lunch_out_msg_block" role="alert">
                                Lunch Out at: <span id="lunch_out_time"></span>
                            </div>

                            <div class="alert alert-danger office_out" id="office_out_msg_block" role="alert">
                                Punch Out at: <span id="office_out_time"></span>
                            </div>

                            <div class="alert alert-warning no_record_msg_block" id="no_record_msg_block" role="alert">
                                No Record
                            </div>

                        </div>

                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header bg-warning">
                            <h3 class="card-title">Holidays</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Holiday Detail</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($holidays)
                                        @foreach ($holidays as $holiday)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $holiday->event_date }}</td>
                                                <td>{{ $holiday->event }}</td>
                                                <td>
                                                    @if ($holiday->status == 'yes')
                                                        <span
                                                            class="badge badge-danger">{{ 'Optional' }}</span>@else{{ '' }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection


@push('footer-script')
    <script>
        // get element of watch
        let watch = document.getElementById("watch");
        // get button of punch in
        let punchInBtn = document.getElementById("punch_in");
        // get button of punch out
        let punchOutBtn = document.getElementById("punch_out");
        let checkBtn = true;
        let timer;

        //___________sec, min, hour

        let timerArr = [0, 0, 0];

        let punch_in_time = "{{ !empty($attendance_detail) ? $attendance_detail->office_in : '' }}";
        let punch_out_time = "{{ !empty($attendance_detail) ? $attendance_detail->office_out : '' }}";
        let now_datetime = "{{ date('Y-m-d H:i:s') }}";


        function secondsToHms(d) {
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var s = Math.floor(d % 3600 % 60);
            return [h, m, s];
        }


        if (punch_in_time != "") {
            punch_in_time = moment(punch_in_time);
            now_datetime = moment(now_datetime);

            var total_seconds = now_datetime.diff(punch_in_time, "seconds");
            let HMS = secondsToHms(total_seconds);
            let hours = HMS[0];
            let minutes = HMS[1];
            let seconds = HMS[2];
            timerArr[2] = hours;
            timerArr[1] = minutes;
            timerArr[0] = seconds;
            if (punch_out_time == "") {
                setTimeout(timerHandler, 1000);
            }
        }


        console.log(punch_in_time);
        console.log(timerArr);

        function timerHandler() {
            timerArr[0]++;
            if (timerArr[0] > 59) {
                timerArr[1]++;
                timerArr[0] = 0;
                if (timerArr[1] > 59) {
                    timerArr[2]++;
                    timerArr[1] = 0;
                }
            }
            for (let i = 0; i < timerArr.length; i++) {
                if (String(timerArr[i]).length < 2)
                    timerArr[i] = "0" + timerArr[i];
                else;
            }
            watch.innerHTML = `${timerArr[2]} : ${timerArr[1]} : ${timerArr[0]}`;
            timer = setTimeout(timerHandler, 1000);
        }


        punchInBtn.addEventListener("click", () => {
            if (checkBtn) {
                timer = setTimeout(timerHandler, 1000);
                checkBtn = false;
                $(this).addClass('disabled');
            }
        });



        if (punchOutBtn) {
            punchOutBtn.addEventListener("click", () => {
                checkBtn = true;
                clearTimeout(timer);
                watch.innerHTML = "00 : 00 : 00";
            });
        }



        function get_formatted_time(date_time) {
            const formatted_time = new Date(date_time).toLocaleTimeString('en-US', {
                hour: "numeric",
                minute: "numeric"
            });
            return formatted_time;
        }


        $(document).ready(function() {
            $('.activity').click(function() {
                var activity_name = $(this).attr('data-value');
                var csrf_token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('activity') }}",
                    data: {
                        _token: csrf_token,
                        activity: activity_name
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function(response) {
                        if (response.status == "ps" || response.status == "pos" || response
                            .status == "lis" || response.status == "los") {
                            $("#activityDetail").css('display', 'block');
                            $("#activityDetailLoad").css('display', 'none');
                            let today_attendance_detail = response.today_attendance_detail;
                            if (today_attendance_detail) {

                                if (today_attendance_detail.office_in) {
                                    $("#office_in_msg_block").css('display', 'block');
                                    $("#office_in_time").html(get_formatted_time(
                                        today_attendance_detail
                                        .office_in));
                                    if (!$("#punch_in").hasClass('pointer-event-none')) {
                                        $("#punch_in").addClass("pointer-event-none");
                                    }
                                }

                                if (today_attendance_detail.office_out) {
                                    $("#office_out_msg_block").css('display', 'block');
                                    $("#office_out_time").html(get_formatted_time(
                                        today_attendance_detail
                                        .office_out));
                                    if (!$("#punch_out").hasClass('pointer-event-none')) {
                                        $("#punch_out").addClass("pointer-event-none");
                                    }
                                }

                                if (today_attendance_detail.lunch_in) {
                                    $("#lunch_in_msg_block").css('display', 'block');
                                    $("#lunch_in_time").html(get_formatted_time(
                                        today_attendance_detail
                                        .lunch_in));
                                    if (!$("#lunch_in").hasClass('pointer-event-none')) {
                                        $("#lunch_in").addClass("pointer-event-none");
                                    }
                                }

                                if (today_attendance_detail.lunch_out) {
                                    $("#lunch_out_msg_block").css('display', 'block');
                                    $("#lunch_out_time").html(get_formatted_time(
                                        today_attendance_detail
                                        .lunch_out));
                                    if (!$("#lunch_out").hasClass('pointer-event-none')) {
                                        $("#lunch_out").addClass("pointer-event-none");
                                    }

                                }



                            }

                        } else {

                        }

                    }
                });
            });
        });
    </script>
@endpush
