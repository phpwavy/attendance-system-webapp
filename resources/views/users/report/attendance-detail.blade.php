@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Users Report')

@section('breadcrumb-title', 'Users Report')

@section('main-content')
    {{-- <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <p class="h3">Filter
                    <p>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">From</label>
                        <input type="date" class="form-control" name="from">
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">To</label>
                        <input type="date" class="form-control" name="to">
                    </div>
                </div>
                <div class="col-md-4 pt-4">
                    <input type="submit" value="Search" class="btn btn-outline-dark font-weight-bolds">
                </div>
            </div>
        </div>
    </section> --}}
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">All Reports</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Punch In</th>
                                        <th>Punch Out</th>
                                        <th>Lunch In</th>
                                        <th>Lunch Out</th>
                                        <th>Total Hours</th>
                                        <th>Created At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($reports as $report)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>

                                            <td>
                                                {{ !empty($report->office_in) ? date('H:i:s', strtotime($report->office_in)) : '' }}
                                            </td>
                                            <td>{{ !empty($report->office_out) ? date('H:i:s', strtotime($report->office_out)) : '' }}
                                            </td>
                                            <td>{{ !empty($report->lunch_in) ? date('H:i:s', strtotime($report->lunch_in)) : '' }}
                                            </td>

                                            <td>{{ !empty($report->lunch_out) ? date('H:i:s', strtotime($report->lunch_out)) : '' }}
                                            </td>

                                            <td>
                                                @if (!empty($report->office_in) && !empty($report->office_out))
                                                    @php
                                                        $diff_time = Carbon\Carbon::parse($report->office_in)->diff(Carbon\Carbon::parse($report->office_out));
                                                        echo $diff_time->d . ' Days ' . $diff_time->h . ' Hours ' . $diff_time->i . ' Minutes ' . $diff_time->s . ' Seconds';
                                                    @endphp
                                                @else
                                                    -
                                                @endif
                                            </td>


                                            <td>{{ !empty($report->created_at) ? date('d-m-Y H:i:s', strtotime($report->created_at)) : '' }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@push('footer-script')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush
