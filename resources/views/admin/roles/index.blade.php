@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Manage Holidays')

@section('breadcrumb-title', 'Holidays')

@section('main-content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-custom-danger alert-dismissible fade show" role="alert">
                    <strong>Please check again.</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session()->has('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('holidays.create') }}" class="btn btn-info btn-sm mb-3">Add</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Manage Roles</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roles as $role)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $role->name }}</td>
                                            <td>
                                                <a href="{{ route('roles.edit', ['role' => $role->id]) }}">
                                                    <span class="badge badge-warning p-2"><i
                                                            class="fas fa-edit"></i></span></a>
                                                {{-- <a href=""><span class="badge badge-primary p-2"><i class="fas fa-eye"></i></span></a> --}}
                                                <a href="javascript:void(0)" id="data-{{ $role->id }}"
                                                    class="delete-user-btn" onclick="confirmDelete({{ $role->id }})">
                                                    <span class="badge badge-danger p-2">
                                                        <i class="fas fa-trash"></i></span>
                                                </a>
                                                <form method="POST"
                                                    action="{{ route('roles.destroy', ['role' => $role->id]) }}"
                                                    id="form-{{ $role->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
@endsection


@push('footer-script')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

        function confirmDelete(item_id) {
            if (confirm("Are you sure you want to delete ? ")) {
                $("#form-" + item_id).submit();
            }
        }
    </script>
@endpush
