@extends('layouts.main')
@push('custom-css')
@endpush
@section('title', 'Add Roles | MyTracker')

@section('breadcrumb-title', 'Add Roles')

@section('main-content')
    <!-- Main content -->
    <section class="content users-form">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-custom-danger alert-dismissible fade show" role="alert">
                    <strong>Please check again.</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session()->has('status'))
                <div class="alert alert-success alert-dismissible">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('roles.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row justify-content-center align-items-center">
                    <!-- form start -->
                    <div class="col-md-12 mt-5">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Roles</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="" class="form-control" name="name" placeholder="Name"
                                                value="{{ old('name') }}"> </textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-dark form-btn float-right">Submit</button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@push('footer-script')
    <script></script>
@endpush
