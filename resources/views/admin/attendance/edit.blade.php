@extends('layouts.main')
@push('custom-css')
@endpush
@section('title', 'Edit Attendance Detail| MyTracker')

@section('breadcrumb-title', 'Edit Attendance Detail')

@section('main-content')
    <!-- Main content -->
    <section class="content users-form">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-custom-danger alert-dismissible fade show" role="alert">
                    <strong>Please check again.</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session()->has('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form  action="{{route('attendance.store')}}" enctype="multipart/form-data" method="POST">
                @csrf
                <div class="row justify-content-center align-items-center">
                    <!-- form start -->
                    <div class="col-md-6 mt-5">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Edit Attendance Detail</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">

                                            <label for="user_id">User</label>
                                            <select  class="form-control" id="users_id" disabled>
                                                <option value="">Please Select</option>
                                                @foreach (App\Models\User::where('role', '!=', 2)->get() as $users)
                                                    <option value="{{ $users->id }}" {{($userAttendanceDetail->user->id == $users->id ) ? 'selected' : '' }}>
                                                        {{ $users->name . ' (' . $users->email . ')' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                             <input type="hidden" name="users_id" value="{{$userAttendanceDetail->user->id}}" >
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Office In</label>
                                            <input type="datetime-local" class="form-control" name="office_in"
                                                id="office_in" value="{{ $userAttendanceDetail->office_in }}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="office_out">Office Out</label>
                                            <input type="datetime-local" class="form-control" name="office_out"
                                                id="office_out" value="{{$userAttendanceDetail->office_out}}">
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="lunch_in">Lunch In</label>
                                            <input type="datetime-local" class="form-control" name="lunch_in" id="lunch_in"
                                                value="{{$userAttendanceDetail->lunch_in}}">
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="lunch_out">Lunch Out</label>
                                            <input type="datetime-local" class="form-control" name="lunch_out"
                                                id="lunch_out" value="{{ $userAttendanceDetail->lunch_out}}">
                                        </div>
                                    </div>



                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-dark form-btn float-right">Update</button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@push('footer-script')
    <script></script>
@endpush
