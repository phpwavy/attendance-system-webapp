@extends('layouts.main')
@push('custom-css')
<style>
    .cursor-pointer {
        cursor: pointer;
    }
</style>
@endpush

@section('title', 'Today Attendance')
@section('breadcrumb-title', 'Today Attendance')
@section('main-content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-custom-danger alert-dismissible fade show" role="alert">
                    <strong>Please check again.</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session()->has('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            {{-- <form action="" method="GET">
                <div class="row">
                    <div class="col-12">
                        <p class="h3">Filter<p>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">From</label>
                            <input type="date" class="form-control" name="from"
                                value="{{ !empty(request()->get('from')) ? request()->get('from') : '' }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">To</label>
                            <input type="date" class="form-control" name="to"
                                value="{{ request()->get('to') ? request()->get('to') : '' }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">search</label>
                            <input type="text" class="form-control" name="q"  value="{{ request()->get('q') ? request()->get('q') : '' }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <input type="submit" value="Search" class="btn btn-outline-dark font-weight-bolds"
                            style="margin-top: 32px;">
                    </div>
                </div>
            </form> --}}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Today Attendance</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Office In</th>
                                        <th>Office out</th>
                                        <th>Lunch IN</th>
                                        <th>Lunch Out</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach ($usersAttendanceDetail as $AttendanceDetail)
                                        <tr>
                                            <td>{{ $loop->iteration }}.</td>
                                            <td>{{ $AttendanceDetail->user->name }}</td>
                                            <td>{{ $AttendanceDetail->user->email}}</td>
                                            <td>
                                            @if($AttendanceDetail->office_in!="")
                                               {{\Carbon\Carbon::parse($AttendanceDetail->office_in)->format('d-m-Y h:i:s')}}
                                            @endif
                                            </td>
                                            <td>
                                            @if ($AttendanceDetail->office_out!="")
                                                {{\Carbon\Carbon::parse($AttendanceDetail->office_out)->format('d-m-Y h:i:s')}}
                                            @endif
                                            </td>
                                            <td>
                                               @if ($AttendanceDetail->lunch_in!="")
                                                {{\Carbon\Carbon::parse($AttendanceDetail->lunch_in)->format('d-m-Y h:i:s')}}
                                               @endif
                                            </td>
                                            <td>
                                                @if ($AttendanceDetail->lunch_out!="")
                                                    {{\Carbon\Carbon::parse($AttendanceDetail->lunch_out)->format('d-m-Y h:i:s')}}
                                                @endif
                                            </td>
                                            <td>
                                                <form method="POST"
                                                    action="{{ route('attendance.destroy', ['id' => $AttendanceDetail->id]) }}"
                                                    id="form-{{ $AttendanceDetail->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                <a href="{{ route('attendances.edit', ['id' => $AttendanceDetail->id]) }}">
                                                    <span class="badge badge-warning p-2"><i class="fas fa-edit"></i></span>
                                                </a>
                                                <span class="cursor-pointer" onclick="DeleteDetail({{$AttendanceDetail->id}});">
                                                    <span class="badge badge-danger p-2"><i class="fas fa-trash"></i></span>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach --}}
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if($user->office_in!="")
                                                    {{$user->office_in}}
                                                @else
                                                    {{'00:00'}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($user->office_out!="")
                                                    {{$user->office_out}}
                                                @else
                                                    {{'00:00'}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($user->lunch_in!="")
                                                    {{$user->lunch_in}}
                                                @else
                                                    {{'00:00'}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($user->lunch_out!="")
                                                    {{$user->lunch_out}}
                                                @else
                                                    {{'00:00'}}
                                                @endif
                                            </td>
                                            {{-- <td>
                                                    <form method="POST"
                                                        action=""
                                                        id="form-">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                @php

                                                @endphp
                                                <a href="{{ route(attendances.edit),[id => $user->id]}}">
                                                    <span class="badge badge-warning p-2"><i class="fas fa-edit"></i></span>
                                                </a>
                                                <span class="cursor-pointer" onclick="DeleteDetail();">
                                                    <span class="badge badge-danger p-2"><i class="fas fa-trash"></i></span>
                                                </span>
                                            </td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
@endsection
@push('footer-script')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
        // function DeleteDetail(id){
        //     if (confirm("Are you sure you want to delete ? ")) {
        //         $('#form-'+id).submit();
        //     }
        // }
    </script>
@endpush
