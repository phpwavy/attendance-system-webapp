@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Manage Holidays')

@section('breadcrumb-title', 'Holidays')

@section('main-content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-custom-danger alert-dismissible fade show" role="alert">
                    <strong>Please check again.</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session()->has('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form action="" method="GET">
                <div class="row">
                    <div class="col-12">
                        <p class="h3">Filter
                        <p>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">From</label>
                            <input type="date" class="form-control" name="from"
                                value="{{ !empty(request()->get('from')) ? request()->get('from') : '' }}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">To</label>
                            <input type="date" class="form-control" name="to"
                                value="{{ request()->get('to') ? request()->get('to') : '' }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="submit" value="Search" class="btn btn-outline-dark font-weight-bolds"
                            style="margin-top: 32px;">
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('holidays.create') }}" class="btn btn-info btn-sm mb-3">Add Holidays</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Manage Holidays</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Event Date</th>
                                        <th>Holiday Detail</th>
                                        <th>Optional</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($holidays as $holiday)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{\Carbon\Carbon::parse($holiday->event_date)->format('d-m-Y')}}</td>
                                            <td>{{ $holiday->event }}</td>
                                            <td>
                                                {{ ucfirst($holiday->status) }}
                                            </td>
                                            <td>
                                                <a href="{{ route('holidays.edit', ['holiday' => $holiday->id]) }}">
                                                    <span class="badge badge-warning p-2"><i
                                                            class="fas fa-edit"></i></span></a>
                                                {{-- <a href=""><span class="badge badge-primary p-2"><i class="fas fa-eye"></i></span></a> --}}
                                                <a href="javascript:void(0)" id="data-{{ $holiday->id }}"
                                                    class="delete-user-btn" onclick="confirmDelete({{ $holiday->id }})">
                                                    <span class="badge badge-danger p-2">
                                                        <i class="fas fa-trash"></i></span>
                                                </a>
                                                <form method="POST"
                                                    action="{{ route('holidays.destroy', ['holiday' => $holiday->id]) }}"
                                                    id="form-{{ $holiday->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
@endsection


@push('footer-script')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

        function confirmDelete(item_id) {
            if (confirm("Are you sure you want to delete ? ")) {
                $("#form-" + item_id).submit();
            }
        }
    </script>
@endpush
