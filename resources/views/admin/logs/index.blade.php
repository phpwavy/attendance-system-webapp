@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Users Log')

@section('breadcrumb-title', 'Users Log')

@section('main-content')
    <section class="content">
        <div class="container-fluid">
            <form action="" method="GET">
                <div class="row">
                    <div class="col-12">
                        <p class="h3">Filter
                        <p>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">From</label>
                            <input type="date" class="form-control" name="from"
                                value="{{ !empty(request()->get('from')) ? request()->get('from') : '' }}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">To</label>
                            <input type="date" class="form-control" name="to"
                                value="{{ request()->get('to') ? request()->get('to') : '' }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="submit" value="Search" class="btn btn-outline-dark font-weight-bolds"
                            style="margin-top: 32px;">
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="content mt-3">
        <!--.container-fluid -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">All Logs</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Ip Address</th>
                                        <th>Device Id</th>
                                        <th>Created At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($all_logs as $log)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                @if ($log->User()->exists())
                                                    {{ $log->User->name }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($log->User()->exists())
                                                    {{ $log->User->email }}
                                                @endif
                                            </td>
                                            <td>{{ $log->ip_address }}</td>
                                            <td>{{ $log->device_id }}</td>
                                            <td>{{ $log->created_at }}</td>
                                        <tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@push('footer-script')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });



        });
    </script>
@endpush
