@extends('layouts.main')
@push('custom-css')
@endpush
@section('title', 'Change Leave Request Status | MyTracker')

@section('breadcrumb-title', 'Change Status')

@section('main-content')
    <!-- Main content -->
    <section class="content users-form">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-custom-danger alert-dismissible fade show" role="alert">
                    <strong>Please check again.</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session()->has('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('update-request-status', ['id' => $leave_request->id]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row justify-content-center align-items-center">
                    <!-- form start -->
                    <div class="col-md-8">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update Status</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="leave_type">Leave Type</label>
                                            <select name="leave_type" id="leave_type" class="form-control" readonly>
                                                <option value="">Please Select</option>
                                                <option value="Full"
                                                    {{ $leave_request->leave_type == 'Full' ? 'selected' : '' }}>Full Day
                                                </option>
                                                <option value="Short"
                                                    {{ $leave_request->leave_type == 'Short' ? 'selected' : '' }}>Short Day
                                                </option>
                                                <option value="Half"
                                                    {{ $leave_request->leave_type == 'Half' ? 'selected' : '' }}>Half Day
                                                </option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="reason">Reason</label>
                                            <textarea name="reason" id="reason" rows="3" class="form-control" readonly>{{ $leave_request->reason }}</textarea>
                                        </div>
                                    </div>

                                    @if ($leave_request->leave_type == 'Full')
                                        <div id="for-fullday" class="col-md-12">
                                            <div class="form-group">
                                                <label for="start_date">Start Date</label>
                                                <input type="date" name="start_date" id="start_date" class="form-control"
                                                    value="{{ $leave_request->start_date }}" readonly>
                                            </div>


                                            <div class="form-group">
                                                <label for="start_date">End Date</label>
                                                <input type="date" name="end_date" id="end_date" class="form-control"
                                                    value="{{ $leave_request->end_date }}" readonly>
                                            </div>
                                        </div>
                                    @endif


                                    @if ($leave_request->leave_type == 'Half')
                                        <div id="for-halfday" class="col-md-12">
                                            <div class="form-group">
                                                <label for="on_date">Date</label>
                                                <input type="date" name="on_half_day_date" id="on_half_day_date"
                                                    class="form-control" value="{{ $leave_request->start_date }}" readonly>
                                            </div>


                                            <div class="form-group">
                                                <label for="which_half">Which Half?</label>
                                                <select name="which_half" id="which_half" class="form-control" readonly>
                                                    <option value="">Please Select</option>
                                                    <option value="first"
                                                        {{ $leave_request->which_half == 'first' ? 'selected' : '' }}>First
                                                    </option>
                                                    <option value="second"
                                                        {{ $leave_request->which_half == 'second' ? 'selected' : '' }}>
                                                        Second
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif


                                    @if ($leave_request->leave_type == 'Short')
                                        <div id="for-shortday" class="col-md-12">

                                            <div class="form-group">
                                                <label for="on_short_day_date">Date</label>
                                                <input type="date" name="on_short_day_date" id="on_short_day_date"
                                                    class="form-control" readonly value="{{ $leave_request->start_date }}">
                                            </div>


                                            <div class="form-group">
                                                <label for="start_time">Start Time</label>
                                                <input type="time" name="start_time" id="start_time" class="form-control"
                                                    readonly value="{{ $leave_request->start_time }}">
                                            </div>


                                            <div class="form-group">
                                                <label for="end_time">End Time</label>
                                                <input type="time" name="end_time" id="end_time" class="form-control"
                                                    readonly value="{{ $leave_request->end_time }}">
                                            </div>
                                        </div>
                                    @endif

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Applied By</label>
                                            <input type="text" name="applied_by" readonly
                                                value="{{ $leave_request->User()->exists() ? $leave_request->User->name . ' (' . $leave_request->User->email . ')' : '' }}"
                                                id="email" class="form-control">
                                        </div>
                                    </div>


                                    @if ($leave_request->status != 5)
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="message">Message</label>
                                                <textarea name="message" id="message" rows="2" class="form-control">{{ $leave_request->msg }}</textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="status" class="status">
                                                    Status
                                                </label>
                                                <select name="status" id="" class="status form-control">
                                                    <option value="1"
                                                        {{ $leave_request->status == 1 ? 'selected' : '' }}>
                                                        Pending</option>
                                                    <option value="2"
                                                        {{ $leave_request->status == 2 ? 'selected' : '' }}>
                                                        Processing</option>
                                                    <option value="3"
                                                        {{ $leave_request->status == 3 ? 'selected' : '' }}>
                                                        Rejected</option>
                                                    <option value="4"
                                                        {{ $leave_request->status == 4 ? 'selected' : '' }}>
                                                        Approved</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif





                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @if ($leave_request->status != 5)
                                    <button type="submit" class="btn btn-dark form-btn float-right">Update</button>
                                @endif
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@push('footer-script')
    <script></script>
@endpush
