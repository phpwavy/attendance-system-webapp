@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Manage Users')

@section('breadcrumb-title', 'Users')

@section('main-content')
    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('users.create') }}" class="btn btn-info btn-sm mb-3">Add</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Manage Users</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone No.</th>
                                        <th>Created At</th>
                                        <th>Deleted At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->UserDetail->phone }}</td>
                                            <td><span class="badge badge-outline-success p-2">{{ $user->created_at }}</span>
                                            </td>
                                            <td><span class="badge badge-outline-danger  p-2">{{ $user->deleted_at }}</span>
                                            </td>
                                            <td>
                                                <a href="{{ route('recover_user', ['id' => $user->id]) }}"
                                                    class="btn btn-secondary btn-sm">Recover</a>
                                                <a href="javascript:void(0)"
                                                    class="btn btn-danger btn-sm permanent-delete-btn"
                                                    onclick="confirmDelete({{ $user->id }})">Remove
                                                    Permanently</a>
                                                <form action="{{ route('remove_user_permanently') }}" method="POST"
                                                    id="form-{{ $user->id }}">
                                                    @csrf
                                                    @method('POST')
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                </form>
                                            </td>

                                        </tr>
                                    @endforeach


                                </tbody>

                            </table>

                        </div>
                        <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
@endsection


@push('footer-script')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });



        });

        function confirmDelete(user_id) {
            if (confirm("Are you sure you want to delete ? ")) {
                $("#form-" + user_id).submit();
            }
        }
    </script>
@endpush
