@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Manage Users')

@section('breadcrumb-title', 'Users')

@section('main-content')
    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('users.create') }}" class="btn btn-info btn-sm mb-3">Add Users <i
                            class="fa fa-plus-circle" aria-hidden="true"></i></a>
                    <a href="{{ route('only_trashed_user') }}" class="btn btn-danger btn-sm mb-3">Trash <i
                            class="fas fa-trash"></i></a>
                </div>

            </div>
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Manage Users</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone No.</th>
                                        <th>Designation</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->UserDetail->phone }}</td>
                                            <td>
                                                @if ($user->Roles()->exists())
                                                    <span class="badge badge-warning">{{ $user->Roles->name }}</span>
                                                @endif
                                            </td>
                                            <td>

                                                @if ($user->status == 1)
                                                    <span class="badge badge-success">Active</span>
                                                @elseif ($user->status == 2)
                                                    <span class="badge badge-danger">Inactive</span>
                                                @endif
                                            </td>

                                            <td>
                                                <a href="{{ route('users.edit', ['user' => $user->id]) }}"><span
                                                        class="badge badge-warning p-2"><i
                                                            class="fas fa-edit"></i></span></a>
                                                <a href="{{ route('users.show', ['user' => $user->id]) }}"><span
                                                        class="badge badge-primary p-2"><i
                                                            class="fas fa-eye"></i></span></a>



                                                <a href="javascript:void(0)" id="user-{{ $user->id }}"
                                                    class="delete-user-btn" onclick="confirmDelete({{ $user->id }})">
                                                    <span class="badge badge-danger p-2">
                                                        <i class="fas fa-trash"></i></span>
                                                </a>

                                                <a href="{{ route('user_logs', ['id' => $user->id]) }}">
                                                    <span class="badge badge-secondary p-2 mt-2">
                                                        <i class="fa fa-file" aria-hidden="true"></i>
                                                    </span>
                                                </a>

                                                <a href="{{ route('user_report', ['id' => $user->id]) }}">
                                                    <span class="badge badge-secondary p-2 mt-2">
                                                        <i class="fa fa-file" aria-hidden="true"></i>
                                                    </span>
                                                </a>

                                                <form method="POST"
                                                    action="{{ route('users.destroy', ['user' => $user->id]) }}"
                                                    id="form-{{ $user->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>




                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
@endsection


@push('footer-script')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });



        });

        function confirmDelete(user_id) {
            if (confirm("Are you sure you want to delete ? ")) {
                $("#form-" + user_id).submit();
            }
        }
    </script>
@endpush
