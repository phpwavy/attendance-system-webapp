@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'View User | MyTracker')

@section('breadcrumb-title', 'View User')

@section('main-content')
    <!-- Main content -->
    <section class="content users-form">
        <div class="container-fluid">
            <div class="row">
                <!-- form start -->
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Account Info</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" name="name" id="name"
                                            placeholder="Enter Name" value="{{ $user->name }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email"
                                            placeholder="Enter email" value="{{ $user->email }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" id="password"
                                            placeholder="Enter Password">

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="phone_no">Phone No.</label>
                                        <input type="tel" class="form-control" name="phone" id="phone_no"
                                            placeholder="Enter Phone No." value="{{ $user->UserDetail->phone }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <select name="status" id="status" class="form-control status">
                                            <option value="1" {{ $user->status == 1 ? 'selected' : '' }}>Active
                                            </option>
                                            <option value="2" {{ $user->status == 2 ? 'selected' : '' }}>Inactive
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">User Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="status">Address</label>
                                        <input type="text" name="address" id="address" class="form-control"
                                            placeholder="Enter Address" value="{{ $user->UserDetail->address }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="profile_image">Profile Image</label>
                                        <input type="file" name="profile_image" id="profile_image" class="form-control"
                                            accept="image/*">
                                        @if ($user->UserDetail()->exists())
                                            @if ($user->UserDetail->profile_image)
                                                <a href="{{ asset($user->UserDetail->profile_image) }}"
                                                    target="_blank">View</a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="emergency_person_name">Emergency Contact Name</label>
                                        <input type="text" name="emergency_contact_name" class="form-control"
                                            id="emergency_person_name" placeholder="Enter Emergency Person Name"
                                            value="{{ $user->UserDetail->emergency_contact_name }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="emergency_person_relation">Emergency Contact Relation</label>
                                        <input type="text" name="emergency_contact_relation" class="form-control"
                                            id="emergency_person_relation" placeholder="Enter Emergency Contact Relation"
                                            value="{{ $user->UserDetail->emergency_contact_relation }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="emergency_person_phone">Emergency Contact Phone No.</label>
                                        <input type="tel" class="form-control" name="emergency_contact_phone"
                                            id="emergency_person_phone_no" placeholder="Enter Emergency Contact Phone No."
                                            value="{{ $user->UserDetail->emergency_contact_phone }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
            </div>
            {{-- <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-dark form-btn">Submit</button>
            </div> --}}
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('footer-script')
    <script></script>
@endpush
