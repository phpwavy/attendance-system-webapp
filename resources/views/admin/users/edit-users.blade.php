@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Edit Users | MyTracker')

@section('breadcrumb-title', 'Edit Users')

@section('main-content')
    <!-- Main content -->
    <section class="content users-form">
        <div class="container-fluid">

            @if ($errors->any())
                <div class="alert alert-custom-danger alert-dismissible fade show" role="alert">
                    <strong>Please check again.</strong>

                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if (session()->has('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif




            <form method="POST" action="{{ route('users.update', ['user' => $user->id]) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <!-- form start -->

                    <!-- left column -->
                    <div class="col-md-6">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Account Info</h3>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name" id="name"
                                                placeholder="Enter Name" value="{{ $user->name }}">
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email" id="email"
                                                placeholder="Enter email" value="{{ $user->email }}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password" id="password"
                                                placeholder="Enter Password">

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="phone_no">Phone No.</label>
                                            <input type="tel" class="form-control" name="phone" id="phone_no"
                                                placeholder="Enter Phone No." value="{{ $user->UserDetail->phone }}">
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="designation">Designation</label>
                                            <select name="roles_id" id="roles_id" class="form-control">
                                                <option value="">Please Select</option>
                                                @foreach (\App\Roles::latest()->get() as $roles)
                                                    <option value="{{ $roles->id }}"
                                                        {{ $user->roles_id == $roles->id ? 'selected' : '' }}>
                                                        {{ $roles->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select name="status" id="status" class="form-control status">
                                                <option value="1" {{ $user->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                                <option value="2" {{ $user->status == 2 ? 'selected' : '' }}>Inactive
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>


                            </div>
                            <!-- /.card-body -->



                        </div>
                        <!-- /.card -->




                    </div>


                    <div class="col-md-6">
                        <!-- general form elements -->
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">User Information</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->

                            <div class="card-body">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select name="country" id="country" class="form-control"
                                                data-country-selected="{{ $user->UserDetail->country }}">

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <select name="state" id="state" class="form-control"
                                                data-state-selected="{{ $user->UserDetail->state }}">

                                            </select>
                                        </div>
                                    </div>



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <select name="city" id="city" class="form-control"
                                                data-city-selected="{{ $user->UserDetail->city }}">

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="status">Address</label>
                                            <input type="text" name="address" id="address" class="form-control"
                                                placeholder="Enter Address" value="{{ $user->UserDetail->address }}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="profile_image">Profile Image</label>
                                            <input type="file" name="profile_image" id="profile_image"
                                                class="form-control" accept="image/*">
                                            @if ($user->UserDetail()->exists())
                                                @if (!empty($user->UserDetail->profile_image))
                                                    {{-- <a href="{{ asset($user->UserDetail->profile_image) }}"
                                                        target="_blank">View</a> --}}
                                                    <div class="text-center mt-4 mb-2">
                                                        <img src="{{ asset($user->UserDetail->profile_image) }}"
                                                            width="150px" height="150px">
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="emergency_person_name">Emergency Contact Name</label>
                                            <input type="text" name="emergency_contact_name" class="form-control"
                                                id="emergency_person_name" placeholder="Enter Emergency Person Name"
                                                value="{{ $user->UserDetail->emergency_contact_name }}">
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="emergency_person_relation">Emergency Contact Relation</label>
                                            <input type="text" name="emergency_contact_relation" class="form-control"
                                                id="emergency_person_relation"
                                                placeholder="Enter Emergency Contact Relation"
                                                value="{{ $user->UserDetail->emergency_contact_relation }}">
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="emergency_person_phone">Emergency Contact Phone No.</label>
                                            <input type="tel" class="form-control" name="emergency_contact_phone"
                                                id="emergency_person_phone_no"
                                                placeholder="Enter Emergency Contact Phone No."
                                                value="{{ $user->UserDetail->emergency_contact_phone }}">
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="join_date">Date of Joining</label>
                                            <input type="date" class="form-control" name="join_date" id="join_date"
                                                value="{{ $user->UserDetail->join_date }}">
                                        </div>
                                    </div>








                                </div>


                            </div>
                            <!-- /.card-body -->



                        </div>
                        <!-- /.card -->




                    </div>
                    <!--/.col (left) -->


                </div>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-dark form-btn">Submit</button>
                </div>

            </form>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@push('footer-script')
    <script>
        let countries = [];
        let states_obj = {};

        let show_previous_value = true;

        let city_filled = "{{ $user->UserDetail->city }}";
        let state_filled = "{{ $user->UserDetail->state }}";
        let country_filled = "{{ $user->UserDetail->country }}";





        // get data of countries, states and cities
        $.getJSON("{{ asset('countries-states-city/countries-states-city.json') }}", function(res) {
            countries = res;
            setCountry();
        });

        console.log(countries);


        // set country to selectbox
        function setCountry() {
            let country_output = `<option value=''>Please Select</option>`;
            let selected = "";
            if (countries.length > 0) {
                for (let i = 0; i < countries.length; i++) {
                    if (country_filled == countries[i].name) {
                        selected = "selected";
                    } else {
                        selected = "";
                    }
                    country_output += `<option value = '${countries[i].name}' ${selected}>${countries[i].name}</option>`;
                }
            }
            $("#country").html(country_output);


            if (state_filled != "") {
                if (show_previous_value) {
                    setState();
                }
            }

            if (city_filled != "") {
                if (show_previous_value) {
                    setCity();
                }
            }

            if (state_filled != "" || city_filled != "") {
                show_previous_value = false;
            }

        }


        function setState() {
            let state_select_option = `<option>Please Select</option>`;
            let city_select_option = `<option>Please Select</option>`;
            if (countries.length > 0) {
                if ($("#country").val() != "") {
                    let select_country_obj = countries.find(item => item.name == $("#country").val());
                    states_obj = select_country_obj.states;
                    let all_states = Object.keys(states_obj);
                    let selected = "";
                    all_states.forEach(function(currentValue, index, arr) {

                        if (state_filled == currentValue) {
                            selected = "selected";
                        } else {
                            selected = "";
                        }


                        state_select_option = state_select_option +
                            `<option value='${currentValue}' ${selected}>${currentValue}</option>`;
                    });

                }
            }

            $("#state").html(state_select_option);
            $("#city").html(city_select_option);
        }

        // on change of country, state will set.
        $("#country").on('change', setState);


        function setCity() {

            let city_select_option = `<option>Please Select</option>`;
            let city_arr = [];
            if (countries.length > 0) {
                if ($("#country").val() != "" && $("#state").val() != "") {
                    console.log(states_obj);
                    if ($("#state").val() in states_obj) {
                        city_arr = states_obj[$("#state").val()];
                    }

                }
            }


            if (city_arr.length > 0) {
                let selected = "";
                city_arr.forEach(function(currentValue, index, arr) {
                    if (city_filled == currentValue) {
                        selected = "selected";
                    } else {
                        selected = "";
                    }
                    city_select_option = city_select_option +
                        `<option value='${currentValue}' ${selected}>${currentValue}</option>`;
                });
            }

            $("#city").html(city_select_option);

        }

        // on change of state will set.
        $("#state").on('change', setCity);
    </script>
@endpush
