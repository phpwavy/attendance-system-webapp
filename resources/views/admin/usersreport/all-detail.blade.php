@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Users Report')

@section('breadcrumb-title', 'Users Report')

@section('main-content')
    <section class="content">
    <form action="" method="GET">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <a href="{{ route('attendance.create') }}" class='btn-info btn mb-3'>Add Attendance</a>
                </div>
                <div class="col-6 float-right">
                    <div class="form-group row">
                        <label for="inputPassword" class="col-form-label text-right">Name</label>
                        <div class="col mr-2">
                        <select class="custom-select" name="id" required>
                                <option value=""></option>
                                @foreach (App\Models\User::all()->where('role','!=',2)->where('status','!=',2) as $user)
                                    <option value="{{$user->id}}" {{ request()->get('id') ? ( request()->get('id')==$user->id ? 'selected': '' ) : '' }}>{{$user->name}}({{$user->email}})</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="inputPassword" class="col-form-label text-right">Date</label>
                        <div class="col mr-2">
                            <select class="custom-select" name="month" required>
                                <option></option>
                                @php
                                    $date = \Carbon\Carbon::today()->toDateString();
                                    $date=\Carbon\Carbon::parse($date)->format('M-Y');
                                @endphp
                                <option value="{{$date}}" {{ request()->get('month') ? ( request()->get('month')==$date ? 'selected': '' ) : '' }}>{{$date}}</option>
                                @for($i=1;$i<=6;$i++)
                                    @php $date=\Carbon\Carbon::now()->startOfMonth()->subMonth($i)->toDateString(); $date=\Carbon\Carbon::parse($date); @endphp
                                    <option value="{{$date->format('M-Y')}}" {{ request()->get('month') ? ( request()->get('month')==$date->format('M-Y') ? 'selected': '' ) : '' }}>{{$date->format('M-Y')}} </option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-sm-1 p-0 mr-3">
                            <input type="submit" value="Search" class="btn btn-outline-dark">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row ">
                {{-- <div class="col-md-3">
                    <div class="form-group">
                        <label for="">From</label>
                        <input type="date" class="form-control" name="from"
                            value="{{ !empty(request()->get('from')) ? request()->get('from') : '' }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">To</label>
                        <input type="date" class="form-control" name="to"
                            value="{{ request()->get('to') ? request()->get('to') : '' }}">
                    </div>
                </div> --}}
                {{-- <div class="col-md-2">
                    <div class="form-group">
                        <label for="">Name</label>
                        <select class="custom-select" name="id" required>
                           <option value=""></option>
                           @foreach (App\Models\User::all()->where('role','!=',2)->where('status','!=',2) as $user)
                             <option value="{{$user->id}}" {{ request()->get('id') ? ( request()->get('id')==$user->id ? 'selected': '' ) : '' }}>{{$user->name}}({{$user->email}})</option>
                           @endforeach
                        </select>
                    </div>
                </div> --}}
                {{-- <div class="col-md-2">
                    <div class="form-group">
                        <label for="">Date</label>
                        <select class="custom-select" name="month" required>
                            <option></option>
                            @php
                               $date = \Carbon\Carbon::today()->toDateString();
                               $date=\Carbon\Carbon::parse($date)->format('M-Y');
                            @endphp
                            <option value="{{$date}}" {{ request()->get('month') ? ( request()->get('month')==$date ? 'selected': '' ) : '' }}>{{$date}}</option>
                            @for($i=1;$i<=6;$i++)
                              @php $date=\Carbon\Carbon::now()->startOfMonth()->subMonth($i)->toDateString(); $date=\Carbon\Carbon::parse($date); @endphp
                              <option value="{{$date->format('M-Y')}}" {{ request()->get('month') ? ( request()->get('month')==$date->format('M-Y') ? 'selected': '' ) : '' }}>{{$date->format('M-Y')}} </option>
                            @endfor
                            <option value="prev_year" {{ request()->get('year') ? ( request()->get('year')=='prev_year' ? 'selected': '' ) : '' }}>Previous Year</option>
                            <option value="next_year" {{ request()->get('year') ? ( request()->get('year')=='next_year' ? 'selected': '' ) : '' }}>Next Year</option>
                            <option value="prev_month" {{ request()->get('year') ? ( request()->get('year')=='prev_month' ? 'selected': '' ) : '' }}>Previous Month</option>
                            <option value="next_month" {{ request()->get('year') ? ( request()->get('year')=='next_month' ? 'selected': '' ) : '' }}>next Month</option>
                        </select>
                    </div>
                </div> --}}
                {{-- <div class="col-md-1 mt-4">
                    <div class="form-group p-0 mt-2">
                       <input type="submit" value="Search" class="btn btn-outline-dark">
                    </div>
                </div> --}}
            </div>
        </div>
    </form>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">All Reports</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name/Email</th>
                                        <th>Punch In</th>
                                        <th>Punch Out</th>
                                        <th>Lunch In</th>
                                        <th>Lunch Out</th>
                                        <th>Total Hours</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($reports as $report)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{$report->User->name}}({{$report->User->email}})</td>
                                            <td>
                                                {{ !empty($report->office_in) ? date('h:i:s', strtotime($report->office_in)) : '' }}
                                            </td>
                                            <td>
                                                {{ !empty($report->office_out) ? date('h:i:s', strtotime($report->office_out)) : '' }}
                                            </td>
                                            <td>
                                                {{ !empty($report->lunch_in) ? date('h:i:s', strtotime($report->lunch_in)) : '' }}
                                            </td>
                                            <td>
                                                {{ !empty($report->lunch_out) ? date('h:i:s', strtotime($report->lunch_out)) : '' }}
                                            </td>
                                            <td>
                                                @if (!empty($report->office_in) && !empty($report->office_out))
                                                    @php
                                                        $diff_time = Carbon\Carbon::parse($report->office_in)->diff(Carbon\Carbon::parse($report->office_out));
                                                        echo $diff_time->h . ' Hours ' . $diff_time->i . ' Minutes ' . $diff_time->s . ' Seconds';
                                                    @endphp
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                {{ !empty($report->created_at) ? date('d-M-Y H:i:s', strtotime($report->created_at)) : '' }}
                                            </td>
                                            <td>
                                                <form method="POST"
                                                    action=""
                                                    id="form-@php echo $report->id @endphp">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>

                                                <a href="{{ route('attendances.edit', ['id' => $report->id]) }}">
                                                    <span class="badge badge-warning p-2"><i class="fas fa-edit"></i></span>
                                                </a>
                                                <span class="cursor-pointer" onclick="DeleteDetail({{$report->id}});">
                                                    <span class="badge badge-danger p-2"><i class="fas fa-trash"></i></span>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@endsection

@push('footer-script')
<script>
    function DeleteDetail(id){
        if (confirm("Are you sure you want to delete ? ")) {
            $('#form-'+id).submit();
        }
    }
</script>
@endpush
