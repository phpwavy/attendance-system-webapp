@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Users Report')

@section('breadcrumb-title', 'Users Report')

@section('main-content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <p class="h3">Filter
                    <p>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">From</label>
                        <input type="date" class="form-control" name="from">
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">To</label>
                        <input type="date" class="form-control" name="to">
                    </div>
                </div>
                <div class="col-md-4 pt-4">
                    <input type="submit" value="Search" class="btn btn-outline-dark font-weight-bolds">
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">All Reports</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Punch In</th>
                                        <th>Punch Out</th>
                                        <th>Lunch In</th>
                                        <th>Lunch Out</th>
                                        {{-- <th>Action</th> --}}
                                        <th>Created At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($reports as $report)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                {{ !empty($report->office_in) ? date('H:i:s', strtotime($report->office_in)) : '' }}
                                            </td>
                                            <td>{{ !empty($report->office_out) ? date('H:i:s', strtotime($report->office_out)) : '' }}
                                            </td>
                                            <td>{{ !empty($report->lunch_in) ? date('H:i:s', strtotime($report->lunch_in)) : '' }}
                                            </td>

                                            <td>{{ !empty($report->lunch_out) ? date('H:i:s', strtotime($report->lunch_out)) : '' }}
                                            </td>
                                            {{-- <td>
                                                <a href="javascript:void(0)" class="show-break-report-btn"
                                                    data-attendance_id="{{ $report->id }}">
                                                    <span class="badge badge-primary p-2">
                                                        <i class="fas fa-eye"></i>
                                                    </span>
                                                </a>
                                            </td>

                                            <td>

                                            </td> --}}

                                            <td>{{ !empty($report->created_at) ? date('H:i:s', strtotime($report->created_at)) : '' }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal" tabindex="-1" id="break-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Break Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="break-loader">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            style="margin:auto;background:#fff;display:block;" width="50px" height="50px"
                            viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                            <circle cx="50" cy="50" r="43" stroke-width="8" stroke="#000"
                                stroke-dasharray="67.54424205218055 67.54424205218055" fill="none"
                                stroke-linecap="round">
                                <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite"
                                    dur="1s" keyTimes="0;1" values="0 50 50;360 50 50"></animateTransform>
                            </circle>
                        </svg>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Break In</th>
                                <th>Break Out</th>
                                <th>Total Break Time</th>
                            </tr>
                        </thead>
                        <tbody id="break-tbl-tbody">

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('footer-script')
    <script></script>
@endpush
