@extends('layouts.main')
@push('custom-css')
@endpush

@section('title', 'Dashboard')

@section('breadcrumb-title', 'Dashboard')

@section('main-content')
    <!-- Main content -->
    <!-- Main content -->

    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{ $total_users }}</h3>
                            <p>Total Registered Users</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{ $total_active_users }}</h3>

                            <p>Total Active</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>{{ $total_inactive_users }}</h3>

                            <p>Total Inactive</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                {{-- <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>65</h3>

                            <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div> --}}
                <!-- ./col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    {{-- Activity section --}}


    <section class="content">
        <div class="container-fluid">
            <div class="row mt-5">
                {{-- <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Today Activity</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                Punch In At 10:00 Am
                            </div>
                            <div class="alert alert-secondary" role="alert">
                                Lunch In At 01:35 Pm
                            </div>
                            <div class="alert alert-warning" role="alert">
                                Lunch out At 02:30 Pm
                            </div>
                            <div class="alert alert-danger" role="alert">
                                Punch out At 70:05 Am
                            </div>
                            <div class="alert alert-info" role="alert">
                                Break In At 30:00 pm
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div> --}}
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header bg-warning">
                            <h3 class="card-title">Holidays</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Holiday Detail</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($holidays)
                                        @foreach ($holidays as $holiday)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $holiday->event_date }}</td>
                                                <td>{{ $holiday->event }}</td>
                                                <td>
                                                    @if ($holiday->status == 'yes')
                                                        <span
                                                            class="badge badge-danger">{{ 'Optional' }}</span>@else{{ '' }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        -
                                    @endif

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

@push('footer-script')
@endpush
