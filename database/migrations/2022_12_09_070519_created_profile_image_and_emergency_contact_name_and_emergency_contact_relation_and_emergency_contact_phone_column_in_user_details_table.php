<?php

/**
 * Laravel Schematics
 *
 * WARNING: removing @tag value will disable automated removal
 *
 * @package Laravel-schematics
 * @author  Maarten Tolhuijs <mtolhuys@protonmail.com>
 * @url     https://github.com/mtolhuys/laravel-schematics
 * @tag     laravel-schematics-user_details-model
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedProfileImageAndEmergencyContactNameAndEmergencyContactRelationAndEmergencyContactPhoneColumnInUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_details', static function (Blueprint $table) {
            $table->string('profile_image', 255)->nullable();
            $table->string('emergency_contact_name', 255)->nullable();
            $table->string('emergency_contact_relation', 255)->nullable();
            $table->string('emergency_contact_phone', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_details', static function (Blueprint $table) {
            $table->dropColumn('profile_image');
            $table->dropColumn('emergency_contact_name');
            $table->dropColumn('emergency_contact_relation');
            $table->dropColumn('emergency_contact_phone');
        });
    }
}
